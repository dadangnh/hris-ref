<?php

namespace App\DataFixtures;

use App\Entity\Organisasi\Golongan;
use App\Entity\Organisasi\JenisSk;
use App\Entity\Pegawai\Pribadi\Agama;
use App\Entity\Pegawai\Pribadi\JenisKelamin;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        $agamaTest = new Agama();
        $agamaTest->setNama('Agama Test');
        $agamaTest->setLegacyKode(99);
        $manager->persist($agamaTest);

        $kelaminTest = new JenisKelamin();
        $kelaminTest->setNama('Kelamin Test');
        $kelaminTest->setLegacyKode(99);
        $manager->persist($kelaminTest);

        $jenisSkTest = new JenisSk();
        $jenisSkTest->setJnsSK("01");
        $jenisSkTest->setKetSK("-");
        $manager->persist($jenisSkTest);

        $jenisSkTest2 = new JenisSk();
        $jenisSkTest2->setJnsSK("02");
        $jenisSkTest2->setKetSK("SK Kepangkatan");
        $manager->persist($jenisSkTest2);

        $golonganTest = new Golongan();
        $golonganTest->setNama("I/a");
        $golonganTest->setLegacyKode(11);
        $manager->persist($golonganTest);

        $manager->flush();
    }
}
