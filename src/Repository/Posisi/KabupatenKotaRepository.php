<?php

namespace App\Repository\Posisi;

use App\Entity\Posisi\KabupatenKota;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method KabupatenKota|null find($id, $lockMode = null, $lockVersion = null)
 * @method KabupatenKota|null findOneBy(array $criteria, array $orderBy = null)
 * @method KabupatenKota[]    findAll()
 * @method KabupatenKota[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class KabupatenKotaRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, KabupatenKota::class);
    }

    // /**
    //  * @return KabupatenKota[] Returns an array of KabupatenKota objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('k')
            ->andWhere('k.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('k.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?KabupatenKota
    {
        return $this->createQueryBuilder('k')
            ->andWhere('k.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
