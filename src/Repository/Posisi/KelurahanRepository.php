<?php

namespace App\Repository\Posisi;

use App\Entity\Posisi\Kelurahan;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Kelurahan|null find($id, $lockMode = null, $lockVersion = null)
 * @method Kelurahan|null findOneBy(array $criteria, array $orderBy = null)
 * @method Kelurahan[]    findAll()
 * @method Kelurahan[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class KelurahanRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Kelurahan::class);
    }

    // /**
    //  * @return Kelurahan[] Returns an array of Kelurahan objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('k')
            ->andWhere('k.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('k.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Kelurahan
    {
        return $this->createQueryBuilder('k')
            ->andWhere('k.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
