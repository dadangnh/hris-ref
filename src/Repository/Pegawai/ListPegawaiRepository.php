<?php

namespace App\Repository\Pegawai;

use App\Entity\BisdmKlasifikasi;
use App\Entity\BisdmKlu;
use App\Entity\BisdmRankingPerKlu;
use App\Entity\Organisasi\Pangkat;
use App\Entity\Pegawai\ListPegawai;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ListPegawai>
 *
 * @method ListPegawai|null find($id, $lockMode = null, $lockVersion = null)
 * @method ListPegawai|null findOneBy(array $criteria, array $orderBy = null)
 * @method ListPegawai[]    findAll()
 * @method ListPegawai[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ListPegawaiRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ListPegawai::class);
    }

    /**
     * @param $keyData
     * @return mixed
     * @throws NonUniqueResultException
     */
    public function queryListPegawai($keyData): mixed
    {
        $roleName           = $keyData['RoleName'] ?? '';
        $jabatan            = $keyData['Jabatan'] ?? '';
        $taxOfficeCode      = $keyData['TaxOfficeCode'] ?? '';
        $orgUnitCode        = $keyData['OrgUnitCode'] ?? '';
        $grade              = $keyData['Grade'] ?? '';
        $workingExperience  = $keyData['WorkingExperience'] ?? '';
        $rank               = $keyData['Rank'] ?? '';
        $caseWorkload       = $keyData['CaseWorkload'] ?? '';
        $caseComplexity     = $keyData['CaseComplexity'] ?? '';
        $caseType           = $keyData['CaseType'] ?? '';
        $caseCrmQuadrant    = $keyData['CaseCRMQuadrant'] ?? '';
        $caseKLU            = $keyData['CaseKLU'] ?? '';

        $queryku = $this->createQueryBuilder('m');
        $queryku->select([
            'm.userId',
            'm.username',
            'm.pegawaiId',
            'm.namaPegawai as nama',
            'm.nip9',
            'm.nip18',
            'm.nik',
            'm.masaKerja',
            'm.grade',
            'm.pangkatId',
            'm.pangkat',
            'm.tempatLahir',
            'm.tanggalLahir',
            'm.jabatanId',
            'm.jabatan',
            'm.roles as roleName',
            'm.kantorId',
            'm.kantor',
            'm.legacyKodeKpp',
            'm.unitId',
            'm.unit',
            'm.unitLegacyKode',
            'k.kelas as cluster',
            'COALESCE(k.maturityToWork,0) skorBISdm',
            'm.tipeJabatan'
        ])->leftJoin(Pangkat::class,'p',Join::WITH,'m.pangkatId=p.id');

        //set condition for CaseComplexity
        if('' != $caseComplexity){
            if(is_array($caseComplexity)){
                $complexity = array();
                foreach($caseComplexity as $key=>$val){
                    array_push($complexity,strtoupper($val));
                }
            }else{
                $complexity = $caseComplexity;
            }
            $queryku->leftJoin(BisdmKlasifikasi::class,'k',Join::WITH,'m.nip9=k.nip9 and UPPER(k.kelas) in (:cc)')
                ->setParameter('cc',$complexity);
        }else{
            $queryku->leftJoin(BisdmKlasifikasi::class,'k',Join::WITH,'m.nip9=k.nip9');
        }

        //set condition if $caseKLU is Set
        if('' != $caseKLU){
            $subQueryku = $this->createQueryBuilder('kluku')
                ->select('klu.kdKlu13Segmen')
                ->from(BisdmKlu::class, 'klu')
                ->andWhere('klu.kdKlu in (:caseKlu)')
                ->setParameter('caseKlu',$caseKLU)
                ->groupBy('klu.kdKlu13Segmen')
                ->getQuery()->getResult();
            $arrKlu = [];
            foreach($subQueryku as $key => $val){
                array_push($arrKlu,$val['kdKlu13Segmen']);
            }

            $queryku->leftJoin(BisdmRankingPerKlu::class,'rk',Join::WITH,'m.nip9=rk.nip9 and rk.kdKlu13Segmen in (:caseKlu)')
                //->innerJoin(BisdmKlu::class,'klu',Join::WITH,'klu.kdKlu13Segmen=rk.kdKlu13Segmen and klu.kdKlu in (:caseKlu)')
                //->setParameter('caseKlu',$caseKLU);
                ->setParameter('caseKlu',$arrKlu);
        }

        //set condition for $taxOfficeCode
        if(is_array($taxOfficeCode)){
            $queryku->andWhere('m.legacyKodeKpp in (:kodeKpp)')
                ->setParameter('kodeKpp',$taxOfficeCode);
        }else if ('' != $taxOfficeCode){
            $queryku->andWhere('m.legacyKodeKpp = :kodeKpp')
                ->setParameter('kodeKpp',$taxOfficeCode);
        }
        //set condition for $orgUnitCode
        if(is_array($orgUnitCode)){
            $queryku->andWhere('m.unitId in (:kodeOrg)')
                ->setParameter('kodeOrg',$orgUnitCode);
        }else if ('' != $orgUnitCode){
            $queryku->andWhere('m.unitId = :kodeOrg')
                ->setParameter('kodeOrg',$orgUnitCode);
        }

        //set condition for $roleName
        $orWhereTextRole = '';
        if('' != $roleName){
            foreach($roleName as $key=>$val){
                $lowVal = str_replace("'","",strtoupper($val));
                if(0 == $key){
                    $orWhereTextRole .= "UPPER(m.roles) like '%$lowVal%'";
                }else{
                    $orWhereTextRole .= "or UPPER(m.roles) like '%$lowVal%'";
                }
            }
            $queryku->andWhere('('.$orWhereTextRole.')');
        }

        //set condition for $jabatan
        $orWhereTextJab = '';
        if(is_array($jabatan)){
            foreach($jabatan as $key=>$val){
                $lowVal = str_replace("'","",strtolower($val));
                if(0 == $key){
                    $orWhereTextJab .= "lower(m.jabatan) like '%$lowVal%'";
                }else{
                    $orWhereTextJab .= "or lower(m.jabatan) like '%$lowVal%'";
                }
            }
            $queryku->andWhere('('.$orWhereTextJab.')');
        }else if ('' != $jabatan){
            $queryku->andWhere('lower(m.jabatan) like :jabatan')
                ->setParameter('jabatan','%'.strtolower($jabatan).'%');
        }

        //set condition for $grade
        if('' != $grade){
            $queryku->andWhere('m.grade >= :grade')
                ->setParameter('grade',$grade);
        }

        //set condition for $workingExperience
        if('' != $workingExperience){
            $queryku->andWhere('m.masaKerja >= :masker')
                ->setParameter('masker',$workingExperience);
        }

        //set condition for $workingExperience
        if('' != $rank){
            $queryku->andWhere('m.pangkat in (:rank)')
                ->setParameter('rank',$rank);
        }

        $queryku->andWhere('m.pensiun = false')
            ->addOrderBy('m.legacyKodeKpp', 'DESC');

        if('' != $caseKLU) {
            $queryku->addOrderBy('rk.ranking', 'ASC')
                ->addOrderBy('skorBISdm', 'DESC');
        }else{
            $queryku->addOrderBy('skorBISdm', 'DESC');
        }
        $queryku->addOrderBy('p.legacyKode', 'DESC')
            ->addOrderBy('m.masaKerja','DESC');

        return $queryku->getQuery()->getResult();
    }
}
