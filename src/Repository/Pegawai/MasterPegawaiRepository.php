<?php

namespace App\Repository\Pegawai;

use App\Entity\BisdmKlasifikasi;
use App\Entity\BisdmKlu;
use App\Entity\BisdmRankingPerKlu;
use App\Entity\Organisasi\Pangkat;
use App\Entity\Pegawai\MasterPegawai;
use App\Entity\Pegawai\PegawaiPlh;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MasterPegawai|null find($id, $lockMode = null, $lockVersion = null)
 * @method MasterPegawai|null findOneBy(array $criteria, array $orderBy = null)
 * @method MasterPegawai[]    findAll()
 * @method MasterPegawai[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MasterPegawaiRepository extends ServiceEntityRepository
{
//    /**
//     * @param ManagerRegistry $registry
//     */
//    public function __construct(ManagerRegistry $registry)
//    {
//        parent::__construct($registry, MasterPegawai::class);
//    }

    /**
     * @param EntityManagerInterface $em
     * @param ManagerRegistry $registry
    */
    public function __construct(EntityManagerInterface $em, ManagerRegistry $registry)
    {
        $this->em = $em;
        parent::__construct($registry, MasterPegawai::class);
    }

    /**
     * @param $value
     * @return MasterPegawai|null
     * @throws NonUniqueResultException
     */
    public function findByNip9($value): ?MasterPegawai
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.nip9 = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    /**
     * @param $value
     * @return MasterPegawai|null
     * @throws NonUniqueResultException
     */
    public function findOneByPegawaiId($value): ?MasterPegawai
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.pegawaiId = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    /**
     * Method to count
     * @param $officeCode
     * @return mixed
     */
    public function findPegawaiCountByJabatanFromOfficeCode($officeCode): mixed
    {
        return $this->createQueryBuilder('m')
            ->select(
                'm.kantorId AS kantor_id',
                'm.kantor',
                'm.jabatanId AS jabatan_id',
                'm.jabatan',
                'count(m.pegawaiId) AS pegawai_count'
            )
            ->andWhere('m.kantorId = :kantorCode')
            ->andWhere('m.pensiun = :pensiun')
            ->groupBy('m.kantorId', 'm.kantor', 'm.jabatanId', 'm.jabatan')
            ->setParameter('kantorCode', $officeCode)
            ->setParameter('pensiun', false)
            ->orderBy('m.jabatan', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * Method to count
     * @param $officeCode
     * @return mixed
     */
    public function findPegawaiCountByGolonganFromOfficeCode($officeCode): mixed
    {
        return $this->createQueryBuilder('m')
            ->select(
                'm.kantorId AS kantor_id',
                'm.kantor',
                'm.pangkatId AS pangkat_id',
                'm.pangkat',
                'count(m.pegawaiId) AS pegawai_count'
            )
            ->andWhere('m.kantorId = :kantorCode')
            ->andWhere('m.pensiun = :pensiun')
            ->groupBy('m.kantorId', 'm.kantor', 'm.pangkatId', 'm.pangkat')
            ->setParameter('kantorCode', $officeCode)
            ->setParameter('pensiun', false)
            ->orderBy('m.pangkat', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * Method to count
     * @param $officeCode
     * @return mixed
     */
    public function findPegawaiCountByPendidikanFromOfficeCode($officeCode): mixed
    {
        return $this->createQueryBuilder('m')
            ->select(
                'm.kantorId AS kantor_id',
                'm.kantor',
                'm.jenjangPendidikanId AS pendidikan_id',
                'm.jenjangPendidikan AS pendidikan',
                'count(m.pegawaiId) AS pegawai_count'
            )
            ->andWhere('m.kantorId = :kantorCode')
            ->andWhere('m.pensiun = :pensiun')
            ->groupBy('m.kantorId', 'm.kantor', 'm.jenjangPendidikanId', 'm.jenjangPendidikan')
            ->setParameter('kantorCode', $officeCode)
            ->setParameter('pensiun', false)
            ->orderBy('m.jenjangPendidikan', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return mixed
     */
    public function findPegawaiByJabatanNationWide(): mixed
    {
        return $this->createQueryBuilder('m')
            ->select(
                'm.jabatanId AS jabatan_id',
                'm.jabatan',
                'count(m.pegawaiId) AS pegawai_count'
            )
            ->andWhere('m.pensiun = :pensiun')
            ->groupBy('m.jabatanId', 'm.jabatan')
            ->setParameter('pensiun', false)
            ->orderBy('m.jabatan', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return mixed
     */
    public function findPegawaiByGolonganNationWide(): mixed
    {
        return $this->createQueryBuilder('m')
            ->select(
                'm.pangkatId AS pangkat_id',
                'm.pangkat',
                'count(m.pegawaiId) AS pegawai_count'
            )
            ->andWhere('m.pensiun = :pensiun')
            ->groupBy('m.pangkatId', 'm.pangkat')
            ->setParameter('pensiun', false)
            ->orderBy('m.pangkat', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return mixed
     */
    public function findPegawaiByPendidikanNationWide(): mixed
    {
        return $this->createQueryBuilder('m')
            ->select(
                'm.jenjangPendidikanId AS pendidikan_id',
                'm.jenjangPendidikan AS pendidikan',
                'count(m.pegawaiId) AS pegawai_count'
            )
            ->andWhere('m.pensiun = :pensiun')
            ->groupBy('m.jenjangPendidikanId', 'm.jenjangPendidikan')
            ->setParameter('pensiun', false)
            ->orderBy('m.jenjangPendidikan', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $officeCode
     * @return mixed
     */
    public function findPegawaiByJabatanFromEselon2Code($officeCode): mixed
    {
        return $this->createQueryBuilder('m')
            ->select(
                'm.jabatanId AS jabatan_id',
                'm.jabatan',
                'count(m.pegawaiId) AS pegawai_count'
            )
            ->andWhere('m.kantorEselon2Id = :officeCode')
            ->andWhere('m.pensiun = :pensiun')
            ->groupBy('m.jabatanId', 'm.jabatan')
            ->setParameter('officeCode', $officeCode)
            ->setParameter('pensiun', false)
            ->orderBy('m.jabatan', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $officeCode
     * @return mixed
     */
    public function findPegawaiByGolonganFromEselon2Code($officeCode): mixed
    {
        return $this->createQueryBuilder('m')
            ->select(
                'm.pangkatId AS pangkat_id',
                'm.pangkat',
                'count(m.pegawaiId) AS pegawai_count'
            )
            ->andWhere('m.kantorEselon2Id = :officeCode')
            ->andWhere('m.pensiun = :pensiun')
            ->groupBy('m.pangkatId', 'm.pangkat')
            ->setParameter('officeCode', $officeCode)
            ->setParameter('pensiun', false)
            ->orderBy('m.pangkat', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $officeCode
     * @return mixed
     */
    public function findPegawaiByPendidikanFromEselon2Code($officeCode): mixed
    {
        return $this->createQueryBuilder('m')
            ->select(
                'm.jenjangPendidikanId AS pendidikan_id',
                'm.jenjangPendidikan AS pendidikan',
                'count(m.pegawaiId) AS pegawai_count'
            )
            ->andWhere('m.kantorEselon2Id = :officeCode')
            ->andWhere('m.pensiun = :pensiun')
            ->groupBy('m.jenjangPendidikanId', 'm.jenjangPendidikan')
            ->setParameter('officeCode', $officeCode)
            ->setParameter('pensiun', false)
            ->orderBy('m.jenjangPendidikan', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $id
     * @return mixed
     * @throws NonUniqueResultException
     */
    public function findDistinctKantorDataFromKantorId($id): mixed
    {
        return $this->createQueryBuilder('m')
            ->select('m.kantorId', 'm.kantor')
            ->andWhere('m.kantorId = :id')
            ->setParameter('id', $id)
            ->groupBy('m.kantorId', 'm.kantor')
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param $keyData
     * @return mixed
     * @throws NonUniqueResultException
     */
    public function queryMaster($keyData): mixed
    {
        $roleName           = $keyData['RoleName'] ?? '';
        $jabatan            = $keyData['Jabatan'] ?? '';
        $taxOfficeCode      = $keyData['TaxOfficeCode'] ?? '';
        $orgUnitCode        = $keyData['OrgUnitCode'] ?? '';
        $grade              = $keyData['Grade'] ?? '';
        $workingExperience  = $keyData['WorkingExperience'] ?? '';
        $rank               = $keyData['Rank'] ?? '';
        $caseWorkload       = $keyData['CaseWorkload'] ?? '';
        $caseComplexity     = $keyData['CaseComplexity'] ?? '';
        $caseType           = $keyData['CaseType'] ?? '';
        $caseCrmQuadrant    = $keyData['CaseCRMQuadrant'] ?? '';
        $caseKLU            = $keyData['CaseKLU'] ?? '';

        $queryku = $this->createQueryBuilder('m');
        $queryku->select([
            'm.userId',
            'm.username',
            'm.pegawaiId',
            'm.namaPegawai as nama',
            'm.nip9',
            'm.nip18',
            'm.nik',
            'm.masaKerja',
            'm.grade',
            'm.pangkatId',
            'm.pangkat',
            'm.tempatLahir',
            'm.tanggalLahir',
            'm.jabatanId',
            'm.jabatan',
            'm.roles as roleName',
            'm.kantorId',
            'm.kantor',
            'm.legacyKodeKpp',
            'm.unitId',
            'm.unit',
            'm.unitLegacyKode',
            'k.kelas as cluster',
            'COALESCE(k.maturityToWork,0) skorBISdm'
        ])->leftJoin(Pangkat::class,'p',Join::WITH,'m.pangkatId=p.id');

        //set condition for CaseComplexity
        if('' != $caseComplexity){
            if(is_array($caseComplexity)){
                $complexity = array();
                foreach($caseComplexity as $key=>$val){
                    array_push($complexity,strtoupper($val));
                }
            }else{
                $complexity = $caseComplexity;
            }
            $queryku->leftJoin(BisdmKlasifikasi::class,'k',Join::WITH,'m.nip9=k.nip9 and UPPER(k.kelas) in (:cc)')
                ->setParameter('cc',$complexity);
        }else{
            $queryku->leftJoin(BisdmKlasifikasi::class,'k',Join::WITH,'m.nip9=k.nip9');
        }

        //set condition if $caseKLU is Set
        if('' != $caseKLU){
            $subQueryku = $this->createQueryBuilder('kluku')
                ->select('klu.kdKlu13Segmen')
                ->from(BisdmKlu::class, 'klu')
                ->andWhere('klu.kdKlu in (:caseKlu)')
                ->setParameter('caseKlu',$caseKLU)
                ->groupBy('klu.kdKlu13Segmen')
                ->getQuery()->getResult();
            $arrKlu = [];
            foreach($subQueryku as $key => $val){
                array_push($arrKlu,$val['kdKlu13Segmen']);
            }

            $queryku->leftJoin(BisdmRankingPerKlu::class,'rk',Join::WITH,'m.nip9=rk.nip9 and rk.kdKlu13Segmen in (:caseKlu)')
                //->innerJoin(BisdmKlu::class,'klu',Join::WITH,'klu.kdKlu13Segmen=rk.kdKlu13Segmen and klu.kdKlu in (:caseKlu)')
                //->setParameter('caseKlu',$caseKLU);
                ->setParameter('caseKlu',$arrKlu);
        }

        //set condition for $taxOfficeCode
        if(is_array($taxOfficeCode)){
            $queryku->andWhere('m.legacyKodeKpp in (:kodeKpp)')
                ->setParameter('kodeKpp',$taxOfficeCode);
        }else if ('' != $taxOfficeCode){
            $queryku->andWhere('m.legacyKodeKpp = :kodeKpp')
                ->setParameter('kodeKpp',$taxOfficeCode);
        }
        //set condition for $orgUnitCode
        if(is_array($orgUnitCode)){
            $queryku->andWhere('m.unitId in (:kodeOrg)')
                ->setParameter('kodeOrg',$orgUnitCode);
        }else if ('' != $orgUnitCode){
            $queryku->andWhere('m.unitId = :kodeOrg')
                ->setParameter('kodeOrg',$orgUnitCode);
        }

        //set condition for $roleName
        $orWhereTextRole = '';
        if('' != $roleName){
            foreach($roleName as $key=>$val){
                $lowVal = str_replace("'","",strtoupper($val));
                if(0 == $key){
                    $orWhereTextRole .= "UPPER(m.roles) like '%$lowVal%'";
                }else{
                    $orWhereTextRole .= "or UPPER(m.roles) like '%$lowVal%'";
                }
            }
            $queryku->andWhere('('.$orWhereTextRole.')');
        }

        //set condition for $jabatan
        $orWhereTextJab = '';
        if(is_array($jabatan)){
            foreach($jabatan as $key=>$val){
                $lowVal = str_replace("'","",strtolower($val));
                if(0 == $key){
                    $orWhereTextJab .= "lower(m.jabatan) like '%$lowVal%'";
                }else{
                    $orWhereTextJab .= "or lower(m.jabatan) like '%$lowVal%'";
                }
            }
            $queryku->andWhere('('.$orWhereTextJab.')');
        }else if ('' != $jabatan){
            $queryku->andWhere('lower(m.jabatan) like :jabatan')
                ->setParameter('jabatan','%'.strtolower($jabatan).'%');
        }

        //set condition for $grade
        if('' != $grade){
            $queryku->andWhere('m.grade >= :grade')
                ->setParameter('grade',$grade);
        }

        //set condition for $workingExperience
        if('' != $workingExperience){
            $queryku->andWhere('m.masaKerja >= :masker')
                ->setParameter('masker',$workingExperience);
        }

        //set condition for $workingExperience
        if('' != $rank){
            $queryku->andWhere('m.pangkat in (:rank)')
                ->setParameter('rank',$rank);
        }

        $queryku->andWhere('m.pensiun = false')
            ->addOrderBy('m.legacyKodeKpp', 'DESC');

        if('' != $caseKLU) {
            $queryku->addOrderBy('rk.ranking', 'ASC')
                ->addOrderBy('skorBISdm', 'DESC');
        }else{
            $queryku->addOrderBy('skorBISdm', 'DESC');
        }
        $queryku->addOrderBy('p.legacyKode', 'DESC')
            ->addOrderBy('m.masaKerja','DESC');

//        return $queryku->getQuery()->getSQL();
        return $queryku->getQuery()->getResult();
    }

    /**
     * @param $keyData
     * @return mixed
     * @throws NonUniqueResultException
     */
    public function findEmployeeFromKeyData($keyData): mixed
    {

        /*$rsm = new ResultSetMappingBuilder($this->em);

        $rsm->addScalarResult('id', 'userId', 'uuid');
        $rsm->addScalarResult('id', 'username', 'string');
        $rsm->addScalarResult('id', 'pegawaiId', 'uuid');
        $rsm->addScalarResult('id', 'nama', 'string');
        $rsm->addScalarResult('id', 'nip9', 'string');
        $rsm->addScalarResult('id', 'nip18', 'string');
        $rsm->addScalarResult('id', 'nik', 'string');
        $rsm->addScalarResult('id', 'masaKerja', 'integer');
        $rsm->addScalarResult('id', 'pangkatId', 'uuid');
        $rsm->addScalarResult('id', 'pangkat', 'string');
        $rsm->addScalarResult('id', 'tempatLahir', 'string');
        $rsm->addScalarResult('id', 'tanggalLahir', 'date');
        $rsm->addScalarResult('id', 'jabatanId', 'uuid');
        $rsm->addScalarResult('id', 'jabatan', 'string');
        $rsm->addScalarResult('id', 'roleName', 'text');
        $rsm->addScalarResult('id', 'kantorId', 'uuid');
        $rsm->addScalarResult('id', 'kantor', 'string');
        $rsm->addScalarResult('id', 'legacyKodeKpp', 'string');
        $rsm->addScalarResult('id', 'unitId', 'uuid');
        $rsm->addScalarResult('id', 'unit', 'string');
        $rsm->addScalarResult('id', 'unitLegacyKode', 'string');
        $rsm->addScalarResult('id', 'cluster', 'string');
        $rsm->addScalarResult('id', 'skorBISdm', 'integer');

        $sql = "SELECT * FROM (
             ({$this->queryMaster($keyData)})
         ) tmp
        ";
        $query = $this->em->createNativeQuery($sql, $rsm);*/

//        $this->em->setParameter(1, 'masaKerja');

//        return $query->getScalarResult();

        return $this->queryMaster($keyData);
    }
}
