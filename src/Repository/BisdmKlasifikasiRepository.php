<?php

namespace App\Repository;

use App\Entity\BisdmKlasifikasi;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<BisdmKlasifikasi>
 *
 * @method BisdmKlasifikasi|null find($id, $lockMode = null, $lockVersion = null)
 * @method BisdmKlasifikasi|null findOneBy(array $criteria, array $orderBy = null)
 * @method BisdmKlasifikasi[]    findAll()
 * @method BisdmKlasifikasi[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BisdmKlasifikasiRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BisdmKlasifikasi::class);
    }

    public function save(BisdmKlasifikasi $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(BisdmKlasifikasi $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return BisdmKlasifikasi[] Returns an array of BisdmKlasifikasi objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('b')
//            ->andWhere('b.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('b.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?BisdmKlasifikasi
//    {
//        return $this->createQueryBuilder('b')
//            ->andWhere('b.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
