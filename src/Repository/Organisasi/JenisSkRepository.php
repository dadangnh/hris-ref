<?php

namespace App\Repository\Organisasi;

use App\Entity\Organisasi\JenisSk;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method JenisSk|null find($id, $lockMode = null, $lockVersion = null)
 * @method JenisSk|null findOneBy(array $criteria, array $orderBy = null)
 * @method JenisSk[]    findAll()
 * @method JenisSk[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class JenisSkRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, JenisSk::class);
    }

    // /**
    //  * @return JenisSk[] Returns an array of JenisSk objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('j.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?JenisSk
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
