<?php

namespace App\Repository\Organisasi;

use App\Entity\Organisasi\Golongan;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Golongan|null find($id, $lockMode = null, $lockVersion = null)
 * @method Golongan|null findOneBy(array $criteria, array $orderBy = null)
 * @method Golongan[]    findAll()
 * @method Golongan[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GolonganRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Golongan::class);
    }

    // /**
    //  * @return Golongan[] Returns an array of Golongan objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Golongan
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
