<?php

namespace App\Repository;

use App\Entity\BisdmKlu;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<BisdmKlu>
 *
 * @method BisdmKlu|null find($id, $lockMode = null, $lockVersion = null)
 * @method BisdmKlu|null findOneBy(array $criteria, array $orderBy = null)
 * @method BisdmKlu[]    findAll()
 * @method BisdmKlu[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BisdmKluRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BisdmKlu::class);
    }

    public function save(BisdmKlu $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(BisdmKlu $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return BisdmKlu[] Returns an array of BisdmKlu objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('b')
//            ->andWhere('b.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('b.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?BisdmKlu
//    {
//        return $this->createQueryBuilder('b')
//            ->andWhere('b.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
