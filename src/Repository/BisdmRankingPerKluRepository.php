<?php

namespace App\Repository;

use App\Entity\BisdmRankingPerKlu;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<BisdmRankingPerKlu>
 *
 * @method BisdmRankingPerKlu|null find($id, $lockMode = null, $lockVersion = null)
 * @method BisdmRankingPerKlu|null findOneBy(array $criteria, array $orderBy = null)
 * @method BisdmRankingPerKlu[]    findAll()
 * @method BisdmRankingPerKlu[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BisdmRankingPerKluRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BisdmRankingPerKlu::class);
    }

    public function save(BisdmRankingPerKlu $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(BisdmRankingPerKlu $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return BisdmRankingPerKlu[] Returns an array of BisdmRankingPerKlu objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('b')
//            ->andWhere('b.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('b.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?BisdmRankingPerKlu
//    {
//        return $this->createQueryBuilder('b')
//            ->andWhere('b.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
