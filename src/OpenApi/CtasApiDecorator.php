<?php

namespace App\OpenApi;

use ApiPlatform\OpenApi\Model\Operation;
use ApiPlatform\OpenApi\Model\PathItem;
use ApiPlatform\OpenApi\Model\RequestBody;
use ApiPlatform\OpenApi\OpenApi;
use ApiPlatform\OpenApi\Factory\OpenApiFactoryInterface;
use ArrayObject;

class CtasApiDecorator implements OpenApiFactoryInterface
{
    public function __construct(
        private OpenApiFactoryInterface $decorated
    ) {}

    public function __invoke(array $context = []): OpenApi
    {
        $openApi = ($this->decorated)($context);
        $schemas = $openApi->getComponents()->getSchemas();

        $schemas['CtasRequest'] = new ArrayObject([
            'type' => 'object',
            'properties' => [
                'scope' => [
                    'type' => 'string',
                    'example' => 'national|regional|local',
                ],
                'office_code' => [
                    'type' => 'string',
                    'example' => 'uuid_of_the_office',
                ],
            ],
        ]);

        $schemas['CtasTaskRequest'] = new ArrayObject([
            'type' => 'object',
            'properties' => [
                'RoleName' => [
                    'type' => 'string',
                    'example' => ['ROLE_ACCOUNT_REPRESENTATIVE'],
                ],
                'TaxOfficeCode' => [
                    'type' => 'string',
                    'example' => '507',
                ],
                'OrgUnitCode' => [
                    'type' => 'string',
                    'example' => '38124616-bae2-444d-b24d-0ca21d74a685',
                ],
                'Grade' => [
                    'type' => 'integer',
                    'example' => 10,
                ],
                'WorkingExperience' => [
                    'type' => 'integer',
                    'example' => 28,
                ],
                'Rank' => [
                    'type' => 'string',
                    'example' => 'Penata Muda Tk.I/IIIb',
                ],
                'CaseWorkload' => [
                    'type' => 'integer',
                    'example' => 10,
                ],
                'CaseType' => [
                    'type' => 'string',
                    'example' => 'Jenis Kasus CTAS',
                ],
                'CaseComplexity' => [
                    'type' => 'string',
                    'example' => ['HIGH','MEDIUM','LOW'],
                ],
                'CaseCRMQuadrant' => [
                    'type' => 'string',
                    'example' => 'Kuadran CRM CTAS',
                ],
                'CaseKLU' => [
                    'type' => 'string',
                    'example' => '46335',
                ],
            ],
        ]);

        $schemas['CtasResponse'] = new ArrayObject([
            'type' => 'object',
            'properties' => [
                'code' => [
                    'type' => 'integer',
                    'readOnly' => true,
                ],
                'messages' => [
                    'type' => 'object',
                    'readOnly' => true
                ]
            ],
        ]);

        $schemas['CtasTaskResponse'] = new ArrayObject([
            'type' => 'object',
            'properties' => [
                'code' => [
                    'type' => 'integer',
                    'readOnly' => true,
                ],
                'employee' => [
                    'type' => 'object',
                    'readOnly' => true
                ]
            ],
        ]);

        $countPegawaiByJabatanItem = new PathItem(
            ref: 'MasterPegawai',
            post: new Operation(
                operationId: 'ctasReferenceCountPegawaiByJabatan',
                tags: ['MasterPegawai'],
                responses: [
                    '200' => [
                        'description' => 'Retrieves Count of Employee Record Data Grouped By jabatan',
                        'content'     => [
                            'application/json' => [
                                'schema' => [
                                    '$ref' => '#/components/schemas/CtasResponse',
                                ],
                            ],
                        ],
                    ],
                ],
                summary: 'Retrieves Count of Employee Record Data Grouped By jabatan',
                requestBody: new RequestBody(
                    description: 'Please provide parameters for the request consist of scope (national/ regional/ local)
                                        and uuid of the office_code in case of regional/ local scope chosen',
                    content: new ArrayObject([
                        'application/json' => [
                            'schema' => [
                                '$ref' => '#/components/schemas/CtasRequest',
                            ],
                        ],
                    ]),
                ),
            ),
        );

        $countPegawaiByGolonganItem = new PathItem(
            ref: 'MasterPegawai',
            post: new Operation(
                operationId: 'ctasReferenceCountPegawaiByGolongan',
                tags: ['MasterPegawai'],
                responses: [
                    '200' => [
                        'description' => 'Retrieves Count of Employee Record Data Grouped By golongan/ pangkat',
                        'content'     => [
                            'application/json' => [
                                'schema' => [
                                    '$ref' => '#/components/schemas/CtasResponse',
                                ],
                            ],
                        ],
                    ],
                ],
                summary: 'Retrieves Count of Employee Record Data Grouped By golongan/ pangkat',
                requestBody: new RequestBody(
                    description: 'Please provide parameters for the request consist of scope (national/ regional/ local)
                                        and uuid of the office_code in case of regional/ local scope choosen',
                    content: new ArrayObject([
                        'application/json' => [
                            'schema' => [
                                '$ref' => '#/components/schemas/CtasRequest',
                            ],
                        ],
                    ]),
                ),
            ),
        );

        $countPegawaiByPendidikanItem = new PathItem(
            ref: 'MasterPegawai',
            post: new Operation(
                operationId: 'ctasReferenceCountPegawaiByPendidikan',
                tags: ['MasterPegawai'],
                responses: [
                    '200' => [
                        'description' => 'Retrieves Count of Employee Record Data Grouped By pendidikan',
                        'content'     => [
                            'application/json' => [
                                'schema' => [
                                    '$ref' => '#/components/schemas/CtasResponse',
                                ],
                            ],
                        ],
                    ],
                ],
                summary: 'Retrieves Count of Employee Record Data Grouped By pendidikan',
                requestBody: new RequestBody(
                    description: 'Please provide parameters for the request consist of scope (national/ regional/ local)
                                        and uuid of the office_code in case of regional/ local scope choosen',
                    content: new ArrayObject([
                        'application/json' => [
                            'schema' => [
                                '$ref' => '#/components/schemas/CtasRequest',
                            ],
                        ],
                    ]),
                ),
            ),
        );
        $listPegawaiByRequest = new PathItem(
            ref: 'MasterPegawai',
            post: new Operation(
                operationId: 'ctasReferenceListEmployee',
                tags: ['MasterPegawai'],
                responses: [
                    '200' => [
                        'description' => 'Retrieves List of Employee Record Data',
                        'content'     => [
                            'application/json' => [
                                'schema' => [
                                    '$ref' => '#/components/schemas/CtasTaskResponse',
                                ],
                            ],
                        ],
                    ],
                ],
                summary: 'Retrieves List of Employee Record Data',
                requestBody: new RequestBody(
                    description: 'Please provide parameters for the request',
                    content: new ArrayObject([
                        'application/json' => [
                            'schema' => [
                                '$ref' => '#/components/schemas/CtasTaskRequest',
                            ],
                        ],
                    ]),
                ),
            ),
        );

        $openApi->getPaths()->addPath('/master_pegawais/jumlah_pegawai_jabatan', $countPegawaiByJabatanItem);
        $openApi->getPaths()->addPath('/master_pegawais/jumlah_pegawai_golongan', $countPegawaiByGolonganItem);
        $openApi->getPaths()->addPath('/master_pegawais/jumlah_pegawai_pendidikan', $countPegawaiByPendidikanItem);
        $openApi->getPaths()->addPath('/master_pegawais/get_list_pegawai', $listPegawaiByRequest);

        return $openApi;
    }
}
