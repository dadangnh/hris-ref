<?php

declare(strict_types=1);

namespace App\OpenApi;

use ApiPlatform\OpenApi\Model\Operation;
use ApiPlatform\OpenApi\Model\Parameter;
use ApiPlatform\OpenApi\Model\PathItem;
use ApiPlatform\OpenApi\OpenApi;
use ApiPlatform\OpenApi\Factory\OpenApiFactoryInterface;
use ArrayObject;

final class CustomApiDecorator implements OpenApiFactoryInterface
{
    public function __construct(
        private OpenApiFactoryInterface $decorated
    ) {}

    public function __invoke(array $context = []): OpenApi
    {
        $openApi = ($this->decorated)($context);
        $schemas = $openApi->getComponents()->getSchemas();

        $schemas['GetDataPegByPegawaiIdResponse'] = new ArrayObject([
            'type' => 'object',
            'properties' => [
                'data' => [
                    'type' => 'string',
                    'readOnly' => true,
                ],
            ],
        ]);

        $masterPegawaiByNip = new PathItem(
            ref: 'MasterPegawai',
            get: new Operation(
                operationId: 'getEmployeeDataByNip',
                tags: ['MasterPegawai'],
                responses: [
                    '200' => [
                        'description' => 'Retrieves Employee Record Data From NIP9 or NIP18',
                        'content'     => [
                            'application/json' => [
                                'schema' => [
                                    '$ref' => '#/components/schemas/GetDataPegByPegawaiIdResponse',
                                ],
                            ],
                        ],
                    ],
                ],
                summary: 'Retrieves Employee Record Data From NIP9 or NIP18',
                parameters: [ new Parameter(
                        'nip',
                        'path',
                        'only numeric allowed',
                        true
                    )
                ]
            ),
        );

        $openApi->getPaths()->addPath('/master_pegawais/{nip}/data', $masterPegawaiByNip);

        return $openApi;
    }
}
