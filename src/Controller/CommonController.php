<?php


namespace App\Controller;


use App\Entity\Pegawai\MasterPegawai;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;
use JsonException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Uid\Uuid;

class CommonController extends AbstractController
{
    /**
     * @return JsonResponse
     */
    #[Route('/whoami', name: 'app_whoami', methods: ['POST'])]
    public function whoami(): JsonResponse
    {
        if (!$this->isGranted('ROLE_USER')
            && !$this->isGranted('ROLE_INACTIVE')
            && !$this->isGranted('ROLE_SERVICE_ACCOUNT')
        ) {
            return $this->json([
                'status' => 'error',
                'message' => 'Unauthorized',
            ], 401);
        }

        return $this->json($this->getUser());
    }

    /**
     * @param ManagerRegistry $doctrine
     * @param Request $request
     * @return JsonResponse
     * @throws JsonException
     * @throws NonUniqueResultException
     */
    #[Route('/get_data_by_pegawai_id', methods: ['POST'])]
    public function getDataByPegawaiId(ManagerRegistry $doctrine, Request $request): JsonResponse
    {
        if (!$this->isGranted('ROLE_USER')) {
            return $this->json([
                'code' => 401,
                'error' => 'Unauthorized API access.',
            ], 401);
        }

        $content    = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
        $pegawaiId  = $content['pegawaiId'];

        $manager = $doctrine
            ->getRepository(MasterPegawai::class);

        if (!Uuid::isValid($pegawaiId)) {
            return $this->json([
                'code' => 404,
                'error' => 'pegawaiId format is not valid'
            ], 404);
        }

        $masterPegawaiById  = $manager->findOneByPegawaiId($pegawaiId);

        return $this->json([
            'message'   => 'success',
            'data'      => $masterPegawaiById
        ]);
    }

    /**
     * @param ManagerRegistry $doctrine
     * @param Request $request
     * @return JsonResponse
     * @throws JsonException
     * @throws NonUniqueResultException
     */
    #[Route('/get_data_by_nip9', methods: ['POST'])]
    public function getDataByNip9(ManagerRegistry $doctrine, Request $request): JsonResponse
    {
        if (!$this->isGranted('ROLE_USER')) {
            return $this->json([
                'code' => 401,
                'error' => 'Unauthorized API access.',
            ], 401);
        }

        $content = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
        $nip9    = $content['nip9'];

        /** @var MasterPegawai $masterPegawai  */
        $masterPegawai = $doctrine
            ->getRepository(MasterPegawai::class)
            ->findByNip9($nip9);

        if (null === $masterPegawai) {
            return $this->json([
                'code' => 404,
                'error' => 'Can\'t found employee record data'
            ], 404);
        }

        return $this->json([
            'message' => 'success',
            'data' => $masterPegawai,
        ]);
    }

    /**
     * @param ManagerRegistry $doctrine
     * @param string $nip
     * @return JsonResponse
     */
    #[Route('/master_pegawais/{nip}/data', methods: ['GET'])]
    public function showDataPegawaiByNip(ManagerRegistry $doctrine, string $nip): JsonResponse
    {
        if (!$this->isGranted('ROLE_USER')) {
            return $this->json([
                'code' => 401,
                'error' => 'Unauthorized API access.',
            ], 401);
        }

        if (9 === strlen($nip)) {
            $pegawai = $doctrine
                ->getRepository(MasterPegawai::class)
                ->findOneBy(['nip9' => $nip]);
        } elseif (18 === strlen($nip)) {
            $pegawai = $doctrine
                ->getRepository(MasterPegawai::class)
                ->findOneBy(['nip18' => $nip]);
        } else {
            return $this->json([
                'code' => 404,
                'error' => 'NIP is not valid'
            ], 404);
        }

        if (null === $pegawai) {
            return $this->json([
                'code'  => 404,
                'error' => 'Can\'t found employee record data'
            ],404);
        }

        return $this->json([
            'message' => 'success',
            'data' => $pegawai,
        ]);
    }
}
