<?php

namespace App\Controller;

use App\Entity\Pegawai\ListPegawai;
use App\Entity\Pegawai\MasterPegawai;
use App\Entity\Pegawai\PegawaiLuar;
use App\Entity\Pegawai\PegawaiPlh;
use Doctrine\Persistence\ManagerRegistry;
use JsonException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Uid\Uuid;

class CtasSupportController extends AbstractController
{
    private ManagerRegistry $doctrine;

    public function __construct(ManagerRegistry $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    /**
     * Controller for finding the total pegawai from a specific office by jabatan
     * @throws JsonException
     */
    #[Route('/master_pegawais/jumlah_pegawai_jabatan', methods: ['POST'])]
    public function showPegawaiCountFromMasterPegawaisByJabatan(Request $request): JsonResponse
    {
        // Make sure that the user is logged in
        if (!$this->isGranted('ROLE_USER')
            && !$this->isGranted('ROLE_SERVICE_ACCOUNT')
        ) {
            return $this->json([
                'code' => 401,
                'error' => 'Unauthorized API access.',
            ], 401);
        }

        // Make sure there is content on request
        if (empty($request->getContent())) {
            return $this->json([
                'code' => 422,
                'error' => 'Cannot process empty request.',
            ], 422);
        }

        // Decode the request content
        $content = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
        $scope = $content['scope'];

        // Make sure only three type of parameter allowed
        if (!in_array($scope, ['national', 'regional', 'local'], true)) {
            return $this->json([
                'code' => 422,
                'error' => 'Wrong input on the type field. Only accepted national/ regional/ local as a value.',
            ], 422);
        }

        // For the national wide data
        if ('national' === $scope) {
            $data = $this->doctrine->getRepository(MasterPegawai::class)
                ->findPegawaiByJabatanNationWide();
            return $this->makeJsonOutputNational($data, $scope, 'jabatan');
        }

        $getDataKantor = $this->getKantorDataFromOfficeCode($content['office_code']);
        if (!$getDataKantor['success']) {
            return $this->json($getDataKantor['content'], $getDataKantor['status']);
        }
        $kantor = $getDataKantor['content'];

        // For the regional scope
        if ('regional' === $scope) {
            $data = $this->doctrine->getRepository(MasterPegawai::class)
                ->findPegawaiByJabatanFromEselon2Code($kantor['kantorId']);

            return $this->makeJsonOutput($kantor, $data, $scope, 'jabatan');
        }

        // For the local scope
        if ('local' === $scope) {
            $data = $this->doctrine->getRepository(MasterPegawai::class)
                ->findPegawaiCountByJabatanFromOfficeCode($kantor['kantorId']);
            return $this->makeJsonOutput($kantor, $data, $scope, 'jabatan');
        }

        // Everything else, throw 404
        return $this->json([
            'code' => 404,
            'error' => 'Endpoint with specified arguments is not found.',
        ], 404);
    }

    /**
     * Controller for finding the total pegawai from a specific office by golongan
     * @throws JsonException
     */
    #[Route('/master_pegawais/jumlah_pegawai_golongan', methods: ['POST'])]
    public function showPegawaiCountFromMasterPegawaisByGolongan(Request $request): JsonResponse
    {
        // Make sure that the user is logged in
        if (!$this->isGranted('ROLE_USER')
            && !$this->isGranted('ROLE_SERVICE_ACCOUNT')
        ) {
            return $this->json([
                'code' => 401,
                'error' => 'Unauthorized API access.',
            ], 401);
        }

        if (empty($request->getContent())) {
            return $this->json([
                'code' => 422,
                'error' => 'Cannot process empty request.',
            ], 422);
        }

        // Decode the request content
        $content = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
        $scope = $content['scope'];

        // Make sure only three type of parameter allowed
        if (!in_array($scope, ['national', 'regional', 'local'], true)) {
            return $this->json([
                'code' => 422,
                'error' => 'Wrong input on the type field. Only accepted national/ regional/ local as a value.',
            ], 422);
        }

        // For the national wide data
        if ('national' === $scope) {
            $data = $this->doctrine->getRepository(MasterPegawai::class)
                ->findPegawaiByGolonganNationWide();
            return $this->makeJsonOutputNational($data, $scope, 'pangkat');
        }

        $getDataKantor = $this->getKantorDataFromOfficeCode($content['office_code']);
        if (!$getDataKantor['success']) {
            return $this->json($getDataKantor['content'], $getDataKantor['status']);
        }
        $kantor = $getDataKantor['content'];

        // For the regional scope
        if ('regional' === $scope) {
            $data = $this->doctrine->getRepository(MasterPegawai::class)
                ->findPegawaiByGolonganFromEselon2Code($kantor['kantorId']);

            return $this->makeJsonOutput($kantor, $data, $scope, 'pangkat');
        }

        // For the local scope
        if ('local' === $scope) {
            $data = $this->doctrine->getRepository(MasterPegawai::class)
                ->findPegawaiCountByGolonganFromOfficeCode($kantor['kantorId']);
            return $this->makeJsonOutput($kantor, $data, $scope, 'pangkat');
        }

        // Everything else, throw 404
        return $this->json([
            'code' => 404,
            'error' => 'Endpoint with specified arguments is not found.',
        ], 404);
    }


    /**
     * Controller for finding the total pegawai from a specific office by pendidikan
     * @throws JsonException
     */
    #[Route('/master_pegawais/jumlah_pegawai_pendidikan', methods: ['POST'])]
    public function showPegawaiCountFromMasterPegawaisByPendidikan(Request $request): JsonResponse
    {
        // Make sure that the user is logged in
        if (!$this->isGranted('ROLE_USER')
            && !$this->isGranted('ROLE_SERVICE_ACCOUNT')
        ) {
            return $this->json([
                'code' => 401,
                'error' => 'Unauthorized API access.',
            ], 401);
        }

        if (empty($request->getContent())) {
            return $this->json([
                'code' => 422,
                'error' => 'Cannot process empty request.',
            ], 422);
        }

        // Decode the request content
        $content = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
        $scope = $content['scope'];

        // Make sure only three type of parameter allowed
        if (!in_array($scope, ['national', 'regional', 'local'], true)) {
            return $this->json([
                'code' => 422,
                'error' => 'Wrong input on the type field. Only accepted national/ regional/ local as a value.',
            ], 422);
        }

        // For the national wide data
        if ('national' === $scope) {
            $data = $this->doctrine->getRepository(MasterPegawai::class)
                ->findPegawaiByPendidikanNationWide();
            return $this->makeJsonOutputNational($data, $scope, 'pendidikan');
        }

        $getDataKantor = $this->getKantorDataFromOfficeCode($content['office_code']);
        if (!$getDataKantor['success']) {
            return $this->json($getDataKantor['content'], $getDataKantor['status']);
        }
        $kantor = $getDataKantor['content'];

        // For the regional scope
        if ('regional' === $scope) {
            $data = $this->doctrine->getRepository(MasterPegawai::class)
                ->findPegawaiByPendidikanFromEselon2Code($kantor['kantorId']);

            return $this->makeJsonOutput($kantor, $data, $scope, 'pendidikan');
        }

        // For the local scope
        if ('local' === $scope) {
            $data = $this->doctrine->getRepository(MasterPegawai::class)
                ->findPegawaiCountByPendidikanFromOfficeCode($kantor['kantorId']);
            return $this->makeJsonOutput($kantor, $data, $scope, 'pendidikan');
        }

        // Everything else, throw 404
        return $this->json([
            'code' => 404,
            'error' => 'Endpoint with specified arguments is not found.',
        ], 404);
    }

    /**
     * @param array $kantor
     * @param array $data
     * @param string $scope
     * @param string $type
     * @return JsonResponse
     */
    private function makeJsonOutput(array $kantor, array $data, string $scope, string $type): JsonResponse
    {
        $message = [
            'scope' => $scope,
            'kantor' => $kantor['kantor'],
            'kantorCode' => $kantor['kantorId']
        ];

        return $this->makeJsonOutputGeneric($message, $data, $type);
    }

    /**
     * @param array $data
     * @param string $scope
     * @param string $type
     * @return JsonResponse
     */
    private function makeJsonOutputNational(array $data, string $scope, string $type): JsonResponse
    {
        $message = [
            'scope' => $scope
        ];

        return $this->makeJsonOutputGeneric($message, $data, $type);
    }

    /**
     * @param array $message
     * @param array $data
     * @param string $type
     * @return JsonResponse
     */
    private function makeJsonOutputGeneric(array $message, array $data, string $type): JsonResponse
    {
        $dataJabatan = [];
        foreach ($data as $value) {
            $dataJabatan[] = [$value[$type] => $value];
        }
        $dataJabatan = [$type . 's' => array_merge(...$dataJabatan)];
        $message = array_merge($message, $dataJabatan);

        return $this->json([
            'code' => '200',
            'message' => [
                $message
            ]
        ]);
    }

    /**
     * @param string|null $officeCode
     * @return array
     */
    private function getKantorDataFromOfficeCode(null|string $officeCode): array
    {
        // For the regional and local office type, we need to make sure that the office_code is exist
        if (empty($officeCode)) {
            return [
                'success' => false,
                'content' => [
                    'code' => 422,
                    'error' => 'Please provide office_code parameter.',
                ],
                'status' => 422
            ];
        }

        // Make sure office code is on uuid
        if (!Uuid::isValid($officeCode)) {
            return [
                'success' => false,
                'content' => [
                    'code' => 422,
                    'error' => 'Please provide a valid uuid for the office_code parameter.',
                ],
                'status' => 422
            ];
        }

        // Get the kantor data
        $kantor = $this->doctrine->getRepository(MasterPegawai::class)
            ->findDistinctKantorDataFromKantorId($officeCode);

        // If no kantor exist, throw to user
        if (empty($kantor)) {
            return [
                'success' => false,
                'content' => [
                    'code' => 422,
                    'error' => 'Please provide a valid uuid for the office_code parameter with value of Kantor Id.',
                ],
                'status' => 422
            ];
        }

        return [
            'success' => true,
            'content' => $kantor,
            'status' => 200
        ];
    }

    /**
     * Controller for finding employee list from a specific parameter
     * @throws JsonException
     */
    #[Route('/master_pegawais/get_list_pegawai', methods: ['POST'])]
    public function showPegawaiFromRequest(Request $request): JsonResponse
    {
        // Make sure that the user is logged in
        if (!$this->isGranted('ROLE_USER')
            && !$this->isGranted('ROLE_SERVICE_ACCOUNT')
        ) {
            return $this->json([
                'code' => 401,
                'error' => 'Unauthorized API access.',
            ], 401);
        }

        $keyData = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);

        // Make sure that only valid parameter allowed kode_unit_organisasi
        foreach($keyData as $key=>$val){
            if (!in_array(
                $key,
                [
                    'Jabatan',
                    'RoleName',
                    'TaxOfficeCode',
                    'OrgUnitCode',
                    'Grade',
                    'WorkingExperience',
                    'Rank',
                    'CaseWorkload',
                    'CaseComplexity',
                    'CaseType',
                    'CaseCRMQuadrant',
                    'CaseKLU'
                ],
                true)
            ) {
                return $this->json([
                    'code'  => 404,
                    'error' => 'Invalid key_data parameter',
                    'key'   => $key
                ], 404);
            }
        }

//        $pegawai = $this->doctrine->getRepository(MasterPegawai::class)
//            ->findEmployeeFromKeyData($keyData);

        $listPegawai = $this->doctrine->getRepository(ListPegawai::class)
            ->queryListPegawai($keyData);

        if(0 == count($listPegawai)){
            return $this->json(['code'=>204,'message'=>'No result'],200);
        }

        $response = [
            'code'              => 200,
        ];

        if(0<count($listPegawai)){
            $response['employee'] = $listPegawai;
        }

        /*$keyDataTemper = [];
        if(0 == count($pegawai) && isset($keyData['CaseKLU'])){
            $keyDataTemper['RoleName']          = $keyData['RoleName'] ?? '';
            $keyDataTemper['Jabatan']           = $keyData['Jabatan'] ?? '';
            $keyDataTemper['TaxOfficeCode']     = $keyData['TaxOfficeCode'] ?? '';
            $keyDataTemper['OrgUnitCode']       = $keyData['OrgUnitCode'] ?? '';
            $keyDataTemper['Grade']             = $keyData['Grade'] ?? '';
            $keyDataTemper['WorkingExperience'] = $keyData['WorkingExperience'] ?? '';
            $keyDataTemper['Rank']              = $keyData['Rank'] ?? '';
            $keyDataTemper['CaseComplexity']    = $keyData['CaseComplexity'] ?? '';

            $pegawai = $this->doctrine->getRepository(MasterPegawai::class)
                ->findEmployeeFromKeyData($keyDataTemper);
        }*/

        return $this->json($response,200);
    }
}
