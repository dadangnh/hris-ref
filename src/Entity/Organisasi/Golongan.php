<?php

namespace App\Entity\Organisasi;

use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Serializer\Filter\PropertyFilter;
use ApiPlatform\Doctrine\Orm\Filter\NumericFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use App\Repository\Organisasi\GolonganRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Uid\UuidV4;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Golongan class
 */
#[ApiResource(
    operations: [
        new Get(
            security: 'is_granted("ROLE_USER")',
            securityMessage: 'Only a valid user can access this.'
        ),
        new Put(
            security: 'is_granted("ROLE_APLIKASI") or is_granted("ROLE_ADMIN") or is_granted("ROLE_UPK_PUSAT")',
            securityMessage: 'Only admin/app can add new resource to this entity type.'
        ),
        new Patch(
            security: 'is_granted("ROLE_APLIKASI") or is_granted("ROLE_ADMIN") or is_granted("ROLE_UPK_PUSAT")',
            securityMessage: 'Only admin/app can add new resource to this entity type.'
        ),
        new Delete(
            security: 'is_granted("ROLE_APLIKASI") or is_granted("ROLE_ADMIN") or is_granted("ROLE_UPK_PUSAT")',
            securityMessage: 'Only admin/app can add new resource to this entity type.'
        ),
        new GetCollection(
            security: 'is_granted("ROLE_USER")',
            securityMessage: 'Only a valid user can access this.'
        ),
        new Post(
            security: 'is_granted("ROLE_APLIKASI") or is_granted("ROLE_ADMIN") or is_granted("ROLE_UPK_PUSAT")',
            securityMessage: 'Only admin/app can add new resource to this entity type.'
        )
    ],
    order: [
        'legacyKode' => 'ASC'
    ],
    security: 'is_granted("ROLE_USER")',
    securityMessage: 'Only a valid user can access this.'
)]
#[ORM\Entity(
    repositoryClass: GolonganRepository::class
)]
#[ORM\Table(
    name: 'golongan'
)]
#[ORM\Index(
    columns: [
        'id',
        'nama'
    ],
    name: 'idx_golongan_nama'
)]
#[ORM\Index(
    columns: [
        'id',
        'legacy_kode'
    ],
    name: 'idx_golongan_legacy'
)]
#[ORM\Cache(
    usage: 'NONSTRICT_READ_WRITE'
)]
#[UniqueEntity(
    fields: [
        'nama'
    ]
)]
#[ApiFilter(
    filterClass: SearchFilter::class,
    properties: [
        'id' => 'exact',
        'nama' => 'ipartial',
        'pangkats.id' => 'exact',
        'pangkats.nama' => 'iexact'
    ]
)]
#[ApiFilter(
    filterClass: NumericFilter::class,
    properties: [
        'legacyKode'
    ]
)]
#[ApiFilter(
    filterClass: PropertyFilter::class
)]
class Golongan
{
    #[ORM\Id]
    #[ORM\Column(
        type: 'uuid',
        unique: true
    )]
    #[ORM\Cache(
        usage: 'NONSTRICT_READ_WRITE'
    )]
    private UuidV4 $id;

    #[ORM\Column(
        type: Types::STRING,
        length: 5
    )]
    #[ORM\Cache(
        usage: 'NONSTRICT_READ_WRITE'
    )]
    #[Assert\NotBlank]
    private ?string $nama;

    #[ORM\Column(
        type: Types::INTEGER
    )]
    #[ORM\Cache(
        usage: 'NONSTRICT_READ_WRITE'
    )]
    private ?int $legacyKode;

    #[ORM\OneToOne(
        mappedBy: 'golongan',
        targetEntity: Pangkat::class
    )]
    private ?Pangkat $pangkats;

    public function __construct()
    {
        $this->id = Uuid::v4();
    }

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function getNama(): ?string
    {
        return $this->nama;
    }

    public function setNama(string $nama): self
    {
        $this->nama = $nama;

        return $this;
    }

    public function getLegacyKode(): ?int
    {
        return $this->legacyKode;
    }

    public function setLegacyKode(int $legacyKode): self
    {
        $this->legacyKode = $legacyKode;

        return $this;
    }

    public function getPangkats(): ?Pangkat
    {
        return $this->pangkats;
    }

    public function setPangkats(Pangkat $pangkats): self
    {
        // set the owning side of the relation if necessary
        if ($pangkats->getGolongan() !== $this) {
            $pangkats->setGolongan($this);
        }

        $this->pangkats = $pangkats;

        return $this;
    }
}
