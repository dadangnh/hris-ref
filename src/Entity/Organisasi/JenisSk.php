<?php

namespace App\Entity\Organisasi;

use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Serializer\Filter\PropertyFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use App\Repository\Organisasi\JenisSkRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Uid\UuidV4;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * JenisSK Class
 */
#[ApiResource(
    operations: [
        new Get(
            security: 'is_granted("ROLE_USER")',
            securityMessage: 'Only a valid user can access this.'
        ),
        new Put(
            security: 'is_granted("ROLE_APLIKASI") or is_granted("ROLE_ADMIN") or is_granted("ROLE_UPK_PUSAT")',
            securityMessage: 'Only admin/app can add new resource to this entity type.'
        ),
        new Patch(
            security: 'is_granted("ROLE_APLIKASI") or is_granted("ROLE_ADMIN") or is_granted("ROLE_UPK_PUSAT")',
            securityMessage: 'Only admin/app can add new resource to this entity type.'
        ),
        new Delete(
            security: 'is_granted("ROLE_APLIKASI") or is_granted("ROLE_ADMIN") or is_granted("ROLE_UPK_PUSAT")',
            securityMessage: 'Only admin/app can add new resource to this entity type.'
        ),
        new GetCollection(
            security: 'is_granted("ROLE_USER")',
            securityMessage: 'Only a valid user can access this.'
        ),
        new Post(
            security: 'is_granted("ROLE_APLIKASI") or is_granted("ROLE_ADMIN") or is_granted("ROLE_UPK_PUSAT")',
            securityMessage: 'Only admin/app can add new resource to this entity type.'
        )
    ],
    order: [
        'jnsSK' => 'ASC'
    ],
    security: 'is_granted("ROLE_USER")',
    securityMessage: 'Only a valid user can access this.'
)]
#[ORM\Entity(
    repositoryClass: JenisSkRepository::class
)]
#[ORM\Table(
    name: 'jenis_sk'
)]
#[ORM\Index(
    columns: [
        'id',
        'jns_sk'
    ],
    name: 'idx_jenissk_jnssk'
)]
#[ORM\Index(
    columns: [
        'id',
        'ket_sk'
    ],
    name: 'idx_jenissk_ket'
)]
#[ORM\Cache(
    usage: 'NONSTRICT_READ_WRITE'
)]
#[UniqueEntity(
    fields: [
        'ketSK'
    ]
)]
#[ApiFilter(
    filterClass: SearchFilter::class,
    properties: [
        'id' => 'exact',
        'ketSK' => 'ipartial'
    ]
)]
#[ApiFilter(
    filterClass: PropertyFilter::class
)]
class JenisSk
{
    #[ORM\Id]
    #[ORM\Column(
        type: 'uuid',
        unique: true
    )]
    #[ORM\Cache(
        usage: 'NONSTRICT_READ_WRITE'
    )]
    private UuidV4 $id;

    #[ORM\Column(
        name: 'jns_sk',
        type: Types::STRING,
        length: 2
    )]
    #[ORM\Cache(
        usage: 'NONSTRICT_READ_WRITE'
    )]
    #[Assert\NotBlank]
    private ?string $jnsSK;

    #[ORM\Column(
        name: 'ket_sk',
        type: Types::STRING,
        length: 100
    )]
    #[ORM\Cache(
        usage: 'NONSTRICT_READ_WRITE'
    )]
    #[Assert\NotBlank]
    private ?string $ketSK;

    public function __construct()
    {
        $this->id = Uuid::v4();
    }

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function getJnsSK(): ?string
    {
        return $this->jnsSK;
    }

    public function setJnsSK(?string $jnsSK): self
    {
        $this->jnsSK = $jnsSK;

        return $this;
    }

    public function getKetSK(): ?string
    {
        return $this->ketSK;
    }

    public function setKetSK(string $ketSK): self
    {
        $this->ketSK = $ketSK;

        return $this;
    }
}
