<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Put;
use App\Repository\BisdmKlasifikasiRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(
    repositoryClass: BisdmKlasifikasiRepository::class
)]
#[ApiResource(
    operations: [
        new Get(
            security: 'is_granted("ROLE_USER")',
            securityMessage: 'Only a valid user can access this.'
        ),
        new Put(
            security: 'is_granted("ROLE_APLIKASI") or is_granted("ROLE_ADMIN") or is_granted("ROLE_UPK_PUSAT")',
            securityMessage: 'Only admin/app can add new resource to this entity type.'
        ),
        new Patch(
            security: 'is_granted("ROLE_APLIKASI") or is_granted("ROLE_ADMIN") or is_granted("ROLE_UPK_PUSAT")',
            securityMessage: 'Only admin/app can add new resource to this entity type.'
        ),
        new Delete(
            security: 'is_granted("ROLE_APLIKASI") or is_granted("ROLE_ADMIN") or is_granted("ROLE_UPK_PUSAT")',
            securityMessage: 'Only admin/app can add new resource to this entity type.'
        ),
        new GetCollection(
            security: 'is_granted("ROLE_USER")',
            securityMessage: 'Only a valid user can access this.'
        ),
        new Post(
            security: 'is_granted("ROLE_APLIKASI") or is_granted("ROLE_ADMIN") or is_granted("ROLE_UPK_PUSAT")',
            securityMessage: 'Only admin/app can add new resource to this entity type.'
        )
    ],
    order: [
        'id' => 'ASC'
    ],
    security: 'is_granted("ROLE_USER")',
    securityMessage: 'Only a valid user can access this.'
)]
#[ORM\Table(
    name: 'bisdm_klasifikasi'
)]
#[ORM\Index(
    columns: [
        'id'
    ],
    name: 'idx_bisdm_klasifikasi'
)]
#[ORM\Index(
    columns: [
        'id',
        'nip9'
    ],
    name: 'idx_klasifikasi_kode'
)]
#[ORM\Index(
    columns: [
        'id',
        'nip9',
        'kelas'
    ],
    name: 'idx_klasifikasi_search'
)]
#[ApiFilter(
    filterClass: SearchFilter::class,
    properties: [
        'id' => 'exact',
        'nip9' => 'ipartial',
        'kelas' => 'ipartial',
        'maturityToWork' => 'ipartial',
    ]
)]
class BisdmKlasifikasi
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(
        length: 9
    )]
    private ?string $nip9 = null;

    #[ORM\Column(
        length: 3,
        nullable: true
    )]
    private ?string $kdJabStruktural = null;

    #[ORM\Column(
        length: 4,
        nullable: true
    )]
    private ?string $kdJabFungsional = null;

    #[ORM\Column(
        nullable: true
    )]
    private ?float $maturityToWork = null;

    #[ORM\Column(
        length: 50
    )]
    private ?string $kelas = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNip9(): ?string
    {
        return $this->nip9;
    }

    public function setNip9(string $nip9): self
    {
        $this->nip9 = $nip9;

        return $this;
    }

    public function getKdJabStruktural(): ?string
    {
        return $this->kdJabStruktural;
    }

    public function setKdJabStruktural(?string $kdJabStruktural): self
    {
        $this->kdJabStruktural = $kdJabStruktural;

        return $this;
    }

    public function getKdJabFungsional(): ?string
    {
        return $this->kdJabFungsional;
    }

    public function setKdJabFungsional(?string $kdJabFungsional): self
    {
        $this->kdJabFungsional = $kdJabFungsional;

        return $this;
    }

    public function getMaturityToWork(): ?float
    {
        return $this->maturityToWork;
    }

    public function setMaturityToWork(?float $maturityToWork): self
    {
        $this->maturityToWork = $maturityToWork;

        return $this;
    }

    public function getKelas(): ?string
    {
        return $this->kelas;
    }

    public function setKelas(string $kelas): self
    {
        $this->kelas = $kelas;

        return $this;
    }
}
