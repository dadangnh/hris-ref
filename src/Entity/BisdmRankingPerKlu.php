<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Put;
use App\Repository\BisdmRankingPerKluRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(
    repositoryClass: BisdmRankingPerKluRepository::class
)]
#[ApiResource(
    operations: [
        new Get(
            security: 'is_granted("ROLE_USER")',
            securityMessage: 'Only a valid user can access this.'
        ),
        new Put(
            security: 'is_granted("ROLE_APLIKASI") or is_granted("ROLE_ADMIN") or is_granted("ROLE_UPK_PUSAT")',
            securityMessage: 'Only admin/app can add new resource to this entity type.'
        ),
        new Patch(
            security: 'is_granted("ROLE_APLIKASI") or is_granted("ROLE_ADMIN") or is_granted("ROLE_UPK_PUSAT")',
            securityMessage: 'Only admin/app can add new resource to this entity type.'
        ),
        new Delete(
            security: 'is_granted("ROLE_APLIKASI") or is_granted("ROLE_ADMIN") or is_granted("ROLE_UPK_PUSAT")',
            securityMessage: 'Only admin/app can add new resource to this entity type.'
        ),
        new GetCollection(
            security: 'is_granted("ROLE_USER")',
            securityMessage: 'Only a valid user can access this.'
        ),
        new Post(
            security: 'is_granted("ROLE_APLIKASI") or is_granted("ROLE_ADMIN") or is_granted("ROLE_UPK_PUSAT")',
            securityMessage: 'Only admin/app can add new resource to this entity type.'
        )
    ],
    order: [
        'id' => 'ASC'
    ],
    security: 'is_granted("ROLE_USER")',
    securityMessage: 'Only a valid user can access this.'
)]
#[ORM\Table(
    name: 'bisdm_ranking_per_klu'
)]
#[ORM\Index(
    columns: [
        'id'
    ],
    name: 'idx_bisdm_ranking'
)]
#[ORM\Index(
    columns: [
        'id',
        'nip9',
        'kd_klu13_segmen'
    ],
    name: 'idx_ranking_kode'
)]
#[ApiFilter(
    filterClass: SearchFilter::class,
    properties: [
        'id' => 'exact',
        'nip9' => 'ipartial',
        'ranking' => 'ipartial',
    ]
)]
class BisdmRankingPerKlu
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(
        length: 9
    )]
    private ?string $nip9 = null;

    #[ORM\Column(
        length: 2
    )]
    private ?string $kdKlu13Segmen = null;

    #[ORM\Column]
    private ?int $ranking = null;

    #[ORM\Column(
        type: Types::DATE_MUTABLE,
        nullable: true
    )]
    private ?\DateTimeInterface $dwStartDate = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNip9(): ?string
    {
        return $this->nip9;
    }

    public function setNip9(string $nip9): self
    {
        $this->nip9 = $nip9;

        return $this;
    }

    public function getKdKlu13Segmen(): ?string
    {
        return $this->kdKlu13Segmen;
    }

    public function setKdKlu13Segmen(string $kdKlu13Segmen): self
    {
        $this->kdKlu13Segmen = $kdKlu13Segmen;

        return $this;
    }

    public function getRanking(): ?int
    {
        return $this->ranking;
    }

    public function setRanking(int $ranking): self
    {
        $this->ranking = $ranking;

        return $this;
    }

    public function getDwStartDate(): ?\DateTimeInterface
    {
        return $this->dwStartDate;
    }

    public function setDwStartDate(?\DateTimeInterface $dwStartDate): self
    {
        $this->dwStartDate = $dwStartDate;

        return $this;
    }
}
