<?php

namespace App\Entity\Pegawai;

use ApiPlatform\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Doctrine\Orm\Filter\NumericFilter;
use ApiPlatform\Doctrine\Orm\Filter\RangeFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Serializer\Filter\PropertyFilter;
use App\Repository\Pegawai\ListPegawaiRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Uid\UuidV4;

#[ORM\Entity(
    repositoryClass: ListPegawaiRepository::class
)]
#[ApiResource(
    operations: [
        new Get(
            security: 'is_granted("ROLE_USER")',
            securityMessage: 'Only a valid user can access this.'
        ),
        new Put(
            security: 'is_granted("ROLE_APLIKASI") or is_granted("ROLE_ADMIN") or is_granted("ROLE_UPK_PUSAT")',
            securityMessage: 'Only admin/app can add new resource to this entity type.'
        ),
        new Patch(
            security: 'is_granted("ROLE_APLIKASI") or is_granted("ROLE_ADMIN") or is_granted("ROLE_UPK_PUSAT")',
            securityMessage: 'Only admin/app can add new resource to this entity type.'
        ),
        new Delete(
            security: 'is_granted("ROLE_APLIKASI") or is_granted("ROLE_ADMIN") or is_granted("ROLE_UPK_PUSAT")',
            securityMessage: 'Only admin/app can add new resource to this entity type.'
        ),
        new GetCollection(
            security: 'is_granted("ROLE_USER")',
            securityMessage: 'Only a valid user can access this.'
        ),
        new Post(
            security: 'is_granted("ROLE_APLIKASI") or is_granted("ROLE_ADMIN") or is_granted("ROLE_UPK_PUSAT")',
            securityMessage: 'Only admin/app can add new resource to this entity type.'
        )
    ],
    order: [
        'kantorId' => 'DESC',
        'tipeJabatan' => 'DESC'

    ],
    security: 'is_granted("ROLE_USER")',
    securityMessage: 'Only a valid user can access this.'
)]
#[ORM\Table(
    name: 'list_pegawai'
)]
#[ORM\Index(
    columns: [
        'id',
        'pegawai_id'
    ],
    name: 'idx_list_pegawaiid'
)]
#[ORM\Index(
    columns: [
        'id',
        'nip18',
        'nik'
    ],
    name: 'idx_list_identityx'
)]
#[ORM\Index(
    columns: [
        'id',
        'nip18',
        'nama_pegawai',
        'jabatan',
        'unit',
        'kantor',
        'masa_kerja',
        'grade',
        'jabatan_grade',
        'roles'
    ],
    name: 'idx_list_search'
)]
#[ORM\Index(
    columns: [
        'id',
        'unit',
        'unit_id',
        'unit_legacy_kode',
        'kantor',
        'kantor_id',
        'kantor_legacy_kode'
    ],
    name: 'idx_list_legacy'
)]
#[ORM\Index(
    columns: [
        'id',
        'user_id',
        'username',
        'pegawai_id',
        'nip18',
        'nik'
    ],
    name: 'idx_list_user'
)]
#[ApiFilter(
    filterClass: SearchFilter::class,
    properties: [
        'pegawaiId' => 'exact',
        'namaPegawai' => 'ipartial',
        'kantor' => 'ipartial',
        'unit' => 'ipartial',
        'kantorId' => 'exact',
        'unitId' => 'exact',
        'kantorLegacyKode' => 'exact',
        'unitLegacyKode' => 'exact',
        'pangkat' => 'iexact',
        'pangkatId' => 'exact',
        'jabatan' => 'ipartial',
        'jabatanId' => 'exact',
        'jenisKelamin' => 'exact',
        'jenisKelaminId' => 'exact',
        'nip18' => 'exact',
        'nik' => 'exact',
        'jabatanGrade' => 'iexact',
        'username' => 'iexact',
        'userId' => 'exact',
        'roles' => 'ipartial',
    ]
)]
#[ApiFilter(
    filterClass: BooleanFilter::class,
    properties: [
        'pensiun'
    ]
)]
#[ApiFilter(
    filterClass: NumericFilter::class,
    properties: [
        'grade',
        'masaKerja'
    ]
)]
#[ApiFilter(
    filterClass: RangeFilter::class,
    properties: [
        'grade',
        'masaKerja'
    ]
)]
#[ApiFilter(
    filterClass: PropertyFilter::class
)]
class ListPegawai
{
    #[ORM\Id]
    #[ORM\Column(
        type: 'uuid',
        unique: true
    )]
    private UuidV4 $id;

    #[ORM\Column(
        type: 'uuid'
    )]
    private ?Uuid $pegawaiId = null;

    #[ORM\Column(
        length: 255
    )]
    private ?string $namaPegawai = null;

    #[ORM\Column(
        length: 9,
        nullable: true
    )]
    private ?string $nip9 = null;

    #[ORM\Column(
        length: 18,
        nullable: true
    )]
    private ?string $nip18 = null;

    #[ORM\Column(
        length: 16, nullable: true
    )]
    private ?string $nik = null;

    #[ORM\Column(
        type: 'uuid'
    )]
    private ?Uuid $pangkatId = null;

    #[ORM\Column(
        length: 255
    )]
    private ?string $pangkat = null;

    #[ORM\Column(
        type: 'uuid'
    )]
    private ?Uuid $jabatanId = null;

    #[ORM\Column(
        length: 255
    )]
    private ?string $jabatan = null;

    #[ORM\Column(
        type: 'uuid',
        nullable: true
    )]
    private ?Uuid $unitId = null;

    #[ORM\Column(
        length: 255,
        nullable: true
    )]
    private ?string $unit = null;

    #[ORM\Column(
        type: 'uuid'
    )]
    private ?Uuid $kantorId = null;

    #[ORM\Column(
        length: 255
    )]
    private ?string $kantor = null;

    #[ORM\Column(
        type: 'uuid'
    )]
    private ?Uuid $agamaId = null;

    #[ORM\Column(
        length: 255
    )]
    private ?string $agama = null;

    #[ORM\Column(
        type: 'uuid'
    )]
    private ?Uuid $jenisKelaminId = null;

    #[ORM\Column(
        length: 255
    )]
    private ?string $jenisKelamin = null;

    #[ORM\Column(
        type: Types::DATE_MUTABLE,
        nullable: true
    )]
    private ?\DateTimeInterface $tanggalLahir = null;

    #[ORM\Column(
        length: 255,
        nullable: true
    )]
    private ?string $tempatLahir = null;

    #[ORM\Column]
    private ?bool $pensiun = null;

    #[ORM\Column(
        length: 10,
        nullable: true
    )]
    private ?string $unitLegacyKode = null;

    #[ORM\Column(
        length: 10,
        nullable: true
    )]
    private ?string $kantorLegacyKode = null;

    #[ORM\Column(
        type: 'uuid',
        nullable: true
    )]
    private ?Uuid $jenjangPendidikanId = null;

    #[ORM\Column(
        length: 255,
        nullable: true
    )]
    private ?string $jenjangPendidikan = null;

    #[ORM\Column(
        type: 'uuid',
        nullable: true
    )]
    private ?Uuid $parentKantorId = null;

    #[ORM\Column(
        length: 255,
        nullable: true
    )]
    private ?string $parentKantor = null;

    #[ORM\Column(
        type: 'uuid',
        nullable: true
    )]
    private ?Uuid $parentUnitId = null;

    #[ORM\Column(
        length: 255,
        nullable: true
    )]
    private ?string $parentUnit = null;

    #[ORM\Column(
        type: 'uuid',
        nullable: true
    )]
    private ?Uuid $kantorEselon1Id = null;

    #[ORM\Column(
        length: 255,
        nullable: true
    )]
    private ?string $kantorEselon1 = null;

    #[ORM\Column(
        type: 'uuid',
        nullable: true
    )]
    private ?Uuid $kantorEselon2Id = null;

    #[ORM\Column(
        length: 255,
        nullable: true
    )]
    private ?string $kantorEselon2 = null;

    #[ORM\Column(
        type: 'uuid',
        nullable: true
    )]
    private ?Uuid $kantorEselon3Id = null;

    #[ORM\Column(
        length: 255,
        nullable: true
    )]
    private ?string $kantorEselon3 = null;

    #[ORM\Column(
        length: 255
    )]
    private ?string $username = null;

    #[ORM\Column(
        type: 'uuid'
    )]
    private ?Uuid $userId = null;

    #[ORM\Column(
        length: 255,
        nullable: true
    )]
    private ?string $posisiKelompok = null;

    #[ORM\Column(
        nullable: true
    )]
    private ?int $grade = null;

    #[ORM\Column(
        length: 255,
        nullable: true
    )]
    private ?string $jabatanGrade = null;

    #[ORM\Column(
        nullable: true
    )]
    private ?int $masaKerja = null;

    #[ORM\Column(
        type: Types::SIMPLE_ARRAY
    )]
    private array $roles = [];

    #[ORM\Column(
        length: 3
    )]
    private ?string $legacyKodeKpp = null;

    #[ORM\Column(
        length: 3
    )]
    private ?string $legacyKodeKanwil = null;

    #[ORM\Column(
        length: 50,
        nullable: true
    )]
    private ?string $tipeJabatan = null;

    public function __construct()
    {
        $this->id = Uuid::v4();
    }

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function getPegawaiId(): ?Uuid
    {
        return $this->pegawaiId;
    }

    public function setPegawaiId(Uuid $pegawaiId): static
    {
        $this->pegawaiId = $pegawaiId;

        return $this;
    }

    public function getNamaPegawai(): ?string
    {
        return $this->namaPegawai;
    }

    public function setNamaPegawai(string $namaPegawai): static
    {
        $this->namaPegawai = $namaPegawai;

        return $this;
    }

    public function getNip9(): ?string
    {
        return $this->nip9;
    }

    public function setNip9(?string $nip9): static
    {
        $this->nip9 = $nip9;

        return $this;
    }

    public function getNip18(): ?string
    {
        return $this->nip18;
    }

    public function setNip18(?string $nip18): static
    {
        $this->nip18 = $nip18;

        return $this;
    }

    public function getNik(): ?string
    {
        return $this->nik;
    }

    public function setNik(?string $nik): static
    {
        $this->nik = $nik;

        return $this;
    }

    public function getPangkatId(): ?Uuid
    {
        return $this->pangkatId;
    }

    public function setPangkatId(Uuid $pangkatId): static
    {
        $this->pangkatId = $pangkatId;

        return $this;
    }

    public function getPangkat(): ?string
    {
        return $this->pangkat;
    }

    public function setPangkat(string $pangkat): static
    {
        $this->pangkat = $pangkat;

        return $this;
    }

    public function getJabatanId(): ?Uuid
    {
        return $this->jabatanId;
    }

    public function setJabatanId(Uuid $jabatanId): static
    {
        $this->jabatanId = $jabatanId;

        return $this;
    }

    public function getJabatan(): ?string
    {
        return $this->jabatan;
    }

    public function setJabatan(string $jabatan): static
    {
        $this->jabatan = $jabatan;

        return $this;
    }

    public function getUnitId(): ?Uuid
    {
        return $this->unitId;
    }

    public function setUnitId(?Uuid $unitId): static
    {
        $this->unitId = $unitId;

        return $this;
    }

    public function getUnit(): ?string
    {
        return $this->unit;
    }

    public function setUnit(?string $unit): static
    {
        $this->unit = $unit;

        return $this;
    }

    public function getKantorId(): ?Uuid
    {
        return $this->kantorId;
    }

    public function setKantorId(Uuid $kantorId): static
    {
        $this->kantorId = $kantorId;

        return $this;
    }

    public function getKantor(): ?string
    {
        return $this->kantor;
    }

    public function setKantor(string $kantor): static
    {
        $this->kantor = $kantor;

        return $this;
    }

    public function getAgamaId(): ?Uuid
    {
        return $this->agamaId;
    }

    public function setAgamaId(Uuid $agamaId): static
    {
        $this->agamaId = $agamaId;

        return $this;
    }

    public function getAgama(): ?string
    {
        return $this->agama;
    }

    public function setAgama(string $agama): static
    {
        $this->agama = $agama;

        return $this;
    }

    public function getJenisKelaminId(): ?Uuid
    {
        return $this->jenisKelaminId;
    }

    public function setJenisKelaminId(Uuid $jenisKelaminId): static
    {
        $this->jenisKelaminId = $jenisKelaminId;

        return $this;
    }

    public function getJenisKelamin(): ?string
    {
        return $this->jenisKelamin;
    }

    public function setJenisKelamin(string $jenisKelamin): static
    {
        $this->jenisKelamin = $jenisKelamin;

        return $this;
    }

    public function getTanggalLahir(): ?\DateTimeInterface
    {
        return $this->tanggalLahir;
    }

    public function setTanggalLahir(?\DateTimeInterface $tanggalLahir): static
    {
        $this->tanggalLahir = $tanggalLahir;

        return $this;
    }

    public function getTempatLahir(): ?string
    {
        return $this->tempatLahir;
    }

    public function setTempatLahir(?string $tempatLahir): static
    {
        $this->tempatLahir = $tempatLahir;

        return $this;
    }

    public function isPensiun(): ?bool
    {
        return $this->pensiun;
    }

    public function setPensiun(bool $pensiun): static
    {
        $this->pensiun = $pensiun;

        return $this;
    }

    public function getUnitLegacyKode(): ?string
    {
        return $this->unitLegacyKode;
    }

    public function setUnitLegacyKode(string $unitLegacyKode): static
    {
        $this->unitLegacyKode = $unitLegacyKode;

        return $this;
    }

    public function getKantorLegacyKode(): ?string
    {
        return $this->kantorLegacyKode;
    }

    public function setKantorLegacyKode(?string $kantorLegacyKode): static
    {
        $this->kantorLegacyKode = $kantorLegacyKode;

        return $this;
    }

    public function getJenjangPendidikanId(): ?Uuid
    {
        return $this->jenjangPendidikanId;
    }

    public function setJenjangPendidikanId(?Uuid $jenjangPendidikanId): static
    {
        $this->jenjangPendidikanId = $jenjangPendidikanId;

        return $this;
    }

    public function getJenjangPendidikan(): ?string
    {
        return $this->jenjangPendidikan;
    }

    public function setJenjangPendidikan(?string $jenjangPendidikan): static
    {
        $this->jenjangPendidikan = $jenjangPendidikan;

        return $this;
    }

    public function getParentKantorId(): ?Uuid
    {
        return $this->parentKantorId;
    }

    public function setParentKantorId(?Uuid $parentKantorId): static
    {
        $this->parentKantorId = $parentKantorId;

        return $this;
    }

    public function getParentKantor(): ?string
    {
        return $this->parentKantor;
    }

    public function setParentKantor(?string $parentKantor): static
    {
        $this->parentKantor = $parentKantor;

        return $this;
    }

    public function getParentUnitId(): ?Uuid
    {
        return $this->parentUnitId;
    }

    public function setParentUnitId(?Uuid $parentUnitId): static
    {
        $this->parentUnitId = $parentUnitId;

        return $this;
    }

    public function getParentUnit(): ?string
    {
        return $this->parentUnit;
    }

    public function setParentUnit(?string $parentUnit): static
    {
        $this->parentUnit = $parentUnit;

        return $this;
    }

    public function getKantorEselon1Id(): ?Uuid
    {
        return $this->kantorEselon1Id;
    }

    public function setKantorEselon1Id(?Uuid $kantorEselon1Id): static
    {
        $this->kantorEselon1Id = $kantorEselon1Id;

        return $this;
    }

    public function getKantorEselon1(): ?string
    {
        return $this->kantorEselon1;
    }

    public function setKantorEselon1(?string $kantorEselon1): static
    {
        $this->kantorEselon1 = $kantorEselon1;

        return $this;
    }

    public function getKantorEselon2Id(): ?Uuid
    {
        return $this->kantorEselon2Id;
    }

    public function setKantorEselon2Id(?Uuid $kantorEselon2Id): static
    {
        $this->kantorEselon2Id = $kantorEselon2Id;

        return $this;
    }

    public function getKantorEselon2(): ?string
    {
        return $this->kantorEselon2;
    }

    public function setKantorEselon2(?string $kantorEselon2): static
    {
        $this->kantorEselon2 = $kantorEselon2;

        return $this;
    }

    public function getKantorEselon3Id(): ?Uuid
    {
        return $this->kantorEselon3Id;
    }

    public function setKantorEselon3Id(?Uuid $kantorEselon3Id): static
    {
        $this->kantorEselon3Id = $kantorEselon3Id;

        return $this;
    }

    public function getKantorEselon3(): ?string
    {
        return $this->kantorEselon3;
    }

    public function setKantorEselon3(?string $kantorEselon3): static
    {
        $this->kantorEselon3 = $kantorEselon3;

        return $this;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): static
    {
        $this->username = $username;

        return $this;
    }

    public function getUserId(): ?Uuid
    {
        return $this->userId;
    }

    public function setUserId(Uuid $userId): static
    {
        $this->userId = $userId;

        return $this;
    }

    public function getPosisiKelompok(): ?string
    {
        return $this->posisiKelompok;
    }

    public function setPosisiKelompok(?string $posisiKelompok): static
    {
        $this->posisiKelompok = $posisiKelompok;

        return $this;
    }

    public function getGrade(): ?int
    {
        return $this->grade;
    }

    public function setGrade(?int $grade): static
    {
        $this->grade = $grade;

        return $this;
    }

    public function getJabatanGrade(): ?string
    {
        return $this->jabatanGrade;
    }

    public function setJabatanGrade(?string $jabatanGrade): static
    {
        $this->jabatanGrade = $jabatanGrade;

        return $this;
    }

    public function getMasaKerja(): ?int
    {
        return $this->masaKerja;
    }

    public function setMasaKerja(?int $masaKerja): static
    {
        $this->masaKerja = $masaKerja;

        return $this;
    }

    public function getRoles(): array
    {
        return $this->roles;
    }

    public function setRoles(array $roles): static
    {
        $this->roles = $roles;

        return $this;
    }

    public function getLegacyKodeKpp(): ?string
    {
        return $this->legacyKodeKpp;
    }

    public function setLegacyKodeKpp(string $legacyKodeKpp): static
    {
        $this->legacyKodeKpp = $legacyKodeKpp;

        return $this;
    }

    public function getLegacyKodeKanwil(): ?string
    {
        return $this->legacyKodeKanwil;
    }

    public function setLegacyKodeKanwil(string $legacyKodeKanwil): static
    {
        $this->legacyKodeKanwil = $legacyKodeKanwil;

        return $this;
    }

    public function getTipeJabatan(): ?string
    {
        return $this->tipeJabatan;
    }

    public function setTipeJabatan(?string $tipeJabatan): static
    {
        $this->tipeJabatan = $tipeJabatan;

        return $this;
    }
}
