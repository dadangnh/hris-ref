<?php

namespace App\Entity\Pegawai;

use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Serializer\Filter\PropertyFilter;
use ApiPlatform\Doctrine\Orm\Filter\RangeFilter;
use ApiPlatform\Doctrine\Orm\Filter\NumericFilter;
use ApiPlatform\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use App\Repository\Pegawai\MasterPegawaiRepository;
use DateTimeImmutable;
use DateTimeInterface;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Uid\UuidV4;

/**
 * MasterPegawai Class
 */
#[ApiResource(
    operations: [
        new Get(
            security: 'is_granted("ROLE_USER")',
            securityMessage: 'Only a valid user can access this.'
        ),
        new Put(
            security: 'is_granted("ROLE_APLIKASI") or is_granted("ROLE_ADMIN") or is_granted("ROLE_UPK_PUSAT")',
            securityMessage: 'Only admin/app can add new resource to this entity type.'
        ),
        new Patch(
            security: 'is_granted("ROLE_APLIKASI") or is_granted("ROLE_ADMIN") or is_granted("ROLE_UPK_PUSAT")',
            securityMessage: 'Only admin/app can add new resource to this entity type.'
        ),
        new Delete(
            security: 'is_granted("ROLE_APLIKASI") or is_granted("ROLE_ADMIN") or is_granted("ROLE_UPK_PUSAT")',
            securityMessage: 'Only admin/app can add new resource to this entity type.'
        ),
        new GetCollection(
            security: 'is_granted("ROLE_USER")',
            securityMessage: 'Only a valid user can access this.'
        ),
        new Post(
            security: 'is_granted("ROLE_APLIKASI") or is_granted("ROLE_ADMIN") or is_granted("ROLE_UPK_PUSAT")',
            securityMessage: 'Only admin/app can add new resource to this entity type.'
        )
    ],
    order: [
        'kantor' => 'ASC'
    ],
    security: 'is_granted("ROLE_USER")',
    securityMessage: 'Only a valid user can access this.'
)]
#[ORM\Entity(
    repositoryClass: MasterPegawaiRepository::class
)]
#[ORM\Table(
    name: 'master_pegawai'
)]
#[ORM\Index(
    columns: [
        'id',
        'pegawai_id'
    ],
    name: 'idx_master_pegawaiid'
)]
#[ORM\Index(
    columns: [
        'id',
        'nip9',
        'nip18',
        'nik'
    ],
    name: 'idx_master_identityx'
)]
#[ORM\Index(
    columns: [
        'id',
        'nama_pegawai',
        'jabatan',
        'unit',
        'kantor',
        'jenjang_pendidikan_id',
        'jenjang_pendidikan',
        'jurusan',
        'masa_kerja',
        'grade',
        'jabatan_grade',
        'posisi_kelompok',
        'roles'
    ],
    name: 'idx_master_search'
)]
#[ORM\Index(
    columns: [
        'id',
        'unit',
        'unit_id',
        'unit_legacy_kode',
        'kantor',
        'kantor_id',
        'kantor_legacy_kode'
    ],
    name: 'idx_master_legacy'
)]
#[ORM\Index(
    columns: [
        'id',
        'unit',
        'unit_id',
        'unit_legacy_kode',
        'kantor',
        'kantor_id',
        'kantor_legacy_kode',
        'parent_kantor_id',
        'parent_kantor',
        'parent_unit_id',
        'parent_unit',
        'kantor_eselon1_id',
        'kantor_eselon1',
        'kantor_eselon2_id',
        'kantor_eselon2',
        'kantor_eselon3_id',
        'kantor_eselon3'
    ],
    name: 'idx_master_kantor'
)]
#[ORM\Index(
    columns: [
        'id',
        'user_id',
        'username',
        'pegawai_id',
        'nip9',
        'nip18',
        'nik'
    ],
    name: 'idx_master_user'
)]
#[ORM\Cache(
    usage: 'NONSTRICT_READ_WRITE'
)]
#[UniqueEntity(
    fields: [
        'nip9',
        'nik',
        'nip18',
        'userId',
        'username'
    ]
)]
#[ApiFilter(
    filterClass: SearchFilter::class,
    properties: [
        'pegawaiId' => 'exact',
        'namaPegawai' => 'ipartial',
        'kantor' => 'ipartial',
        'unit' => 'ipartial',
        'kantorId' => 'exact',
        'unitId' => 'exact',
        'kantorLegacyKode' => 'exact',
        'unitLegacyKode' => 'exact',
        'pangkat' => 'iexact',
        'pangkatId' => 'exact',
        'jabatan' => 'ipartial',
        'jabatanId' => 'exact',
        'agama' => 'iexact',
        'agamaId' => 'exact',
        'jenisKelamin' => 'exact',
        'jenisKelaminId' => 'exact',
        'nip9' => 'exact',
        'nip18' => 'exact',
        'nik' => 'exact',
        'jenjangPendidikanId' => 'exact',
        'jenjangPendidikan' => 'iexact',
        'jurusanId' => 'exact',
        'jurusan' => 'iexact',
        'parentKantorId' => 'exact',
        'parentKantor' => 'iexact',
        'parentUnitId' => 'exact',
        'parentUnit' => 'iexact',
        'kantorEselon1Id' => 'exact',
        'kantorEselon1' => 'iexact',
        'kantorEselon2Id' => 'exact',
        'kantorEselon2' => 'iexact',
        'kantorEselon3Id' => 'exact',
        'kantorEselon3' => 'iexact',
        'jabatanGrade' => 'iexact',
        'posisiKelompok' => 'iexact',
        'username' => 'iexact',
        'userId' => 'exact',
        'roles' => 'ipartial',
    ]
)]
#[ApiFilter(
    filterClass: BooleanFilter::class,
    properties: [
        'pensiun'
    ]
)]
#[ApiFilter(
    filterClass: NumericFilter::class,
    properties: [
        'grade',
        'masaKerja'
    ]
)]
#[ApiFilter(
    filterClass: RangeFilter::class,
    properties: [
        'grade',
        'masaKerja'
    ]
)]
#[ApiFilter(
    filterClass: PropertyFilter::class
)]
class MasterPegawai
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(
        type: 'integer'
    )]
    private ?int $id;

    #[ORM\Column(
        type: 'uuid',
        nullable: true
    )]
    private ?Uuid $pegawaiId;

    #[ORM\Column(
        type: Types::STRING,
        length: 255,
        nullable: true
    )]
    private ?string $namaPegawai;

    #[ORM\Column(
        type: Types::STRING,
        length: 9,
        nullable: true
    )]
    private ?string $nip9;

    #[ORM\Column(
        type: Types::STRING,
        length: 18,
        nullable: true
    )]
    private ?string $nip18;

    #[ORM\Column(
        type: Types::STRING,
        length: 16,
        nullable: true
    )]
    private ?string $nik;

    #[ORM\Column(
        type: 'uuid',
        nullable: true
    )]
    private ?Uuid $pangkatId;

    #[ORM\Column(
        type: Types::STRING,
        length: 255,
        nullable: true
    )]
    private ?string $pangkat;

    #[ORM\Column(
        type: 'uuid',
        nullable: true
    )]
    private ?Uuid $jabatanId;

    #[ORM\Column(
        type: Types::STRING,
        length: 255,
        nullable: true
    )]
    private ?string $jabatan;

    #[ORM\Column(
        type: 'uuid',
        nullable: true
    )]
    private ?Uuid $unitId;

    #[ORM\Column(
        type: Types::STRING,
        length: 255,
        nullable: true
    )]
    private ?string $unit;

    #[ORM\Column(
        type: Types::STRING,
        length: 10,
        nullable: true
    )]
    private ?string $unitLegacyKode;

    #[ORM\Column(
        type: 'uuid',
        nullable: true
    )]
    private ?Uuid $kantorId;

    #[ORM\Column(
        type: Types::STRING,
        length: 255,
        nullable: true
    )]
    private ?string $kantor;

    #[ORM\Column(
        type: Types::STRING,
        length: 10,
        nullable: true
    )]
    private ?string $kantorLegacyKode;

    #[ORM\Column(
        type: 'uuid',
        nullable: true
    )]
    private ?UuidV4 $agamaId;

    #[ORM\Column(
        type: Types::STRING,
        length: 255,
        nullable: true
    )]
    private ?string $agama;

    #[ORM\Column(
        type: 'uuid',
        nullable: true
    )]
    private ?UuidV4 $jenisKelaminId;

    #[ORM\Column(
        type: Types::STRING,
        length: 255,
        nullable: true
    )]
    private ?string $jenisKelamin;

    #[ORM\Column(
        type: Types::STRING,
        length: 255,
        nullable: true
    )]
    private ?string $alamatJalan;

    #[ORM\Column(
        type: 'uuid',
        nullable: true
    )]
    private ?UuidV4 $kelurahanId;

    #[ORM\Column(
        type: Types::STRING,
        length: 255,
        nullable: true
    )]
    private ?string $kelurahan;

    #[ORM\Column(
        type: 'uuid',
        nullable: true
    )]
    private ?UuidV4 $kecamatanId;

    #[ORM\Column(
        type: Types::STRING,
        length: 255,
        nullable: true
    )]
    private ?string $kecamatan;

    #[ORM\Column(
        type: 'uuid',
        nullable: true
    )]
    private ?UuidV4 $kabupatenId;

    #[ORM\Column(
        type: Types::STRING,
        length: 255,
        nullable: true
    )]
    private ?string $kabupaten;

    #[ORM\Column(
        type: 'uuid',
        nullable: true
    )]
    private ?UuidV4 $provinsiId;

    #[ORM\Column(
        type: Types::STRING,
        length: 255,
        nullable: true
    )]
    private ?string $provinsi;

    #[ORM\Column(
        type: Types::DATE_MUTABLE,
        nullable: true
    )]
    private ?DateTimeInterface $tanggalLahir;

    #[ORM\Column(
        type: Types::STRING,
        length: 255,
        nullable: true
    )]
    private ?string $tempatLahir;

    #[ORM\Column(
        type: Types::BOOLEAN,
        nullable: true
    )]
    private ?bool $pensiun;

    #[ORM\Column(
        type: 'uuid',
        nullable: true
    )]
    private ?Uuid $jenjangPendidikanId;

    #[ORM\Column(
        type: Types::STRING,
        length: 255,
        nullable: true
    )]
    private ?string $jenjangPendidikan;

    #[ORM\Column(
        type: 'uuid',
        nullable: true
    )]
    private ?Uuid $jurusanId;

    #[ORM\Column(
        type: Types::STRING,
        length: 255,
        nullable: true
    )]
    private ?string $jurusan;

    #[ORM\Column(
        type: Types::DATETIME_IMMUTABLE,
        nullable: true
    )]
    private ?DateTimeImmutable $tglIjazah;

    #[ORM\Column(
        type: 'uuid',
        nullable: true
    )]
    private ?Uuid $parentKantorId;

    #[ORM\Column(
        type: Types::STRING,
        length: 255,
        nullable: true
    )]
    private ?string $parentKantor;

    #[ORM\Column(
        type: 'uuid',
        nullable: true
    )]
    private ?Uuid $parentUnitId;

    #[ORM\Column(
        type: Types::STRING,
        length: 255,
        nullable: true
    )]
    private ?string $parentUnit;

    #[ORM\Column(
        type: 'uuid',
        nullable: true
    )]
    private ?Uuid $kantorEselon1Id;

    #[ORM\Column(
        type: Types::STRING,
        length: 255,
        nullable: true
    )]
    private ?string $kantorEselon1;

    #[ORM\Column(
        type: 'uuid',
        nullable: true
    )]
    private ?Uuid $kantorEselon2Id;

    #[ORM\Column(
        type: Types::STRING,
        length: 255,
        nullable: true
    )]
    private ?string $kantorEselon2;

    #[ORM\Column(
        type: 'uuid',
        nullable: true
    )]
    private ?Uuid $kantorEselon3Id;

    #[ORM\Column(
        type: Types::STRING,
        length: 255,
        nullable: true
    )]
    private ?string $kantorEselon3;

    #[ORM\Column(
        length: 255
    )]
    private ?string $username = null;

    #[ORM\Column(
        type: 'uuid'
    )]
    private ?Uuid $userId = null;

    #[ORM\Column(
        length: 255,
        nullable: true
    )]
    private ?string $posisiKelompok = null;

    #[ORM\Column(
        nullable: true
    )]
    private ?int $grade = null;

    #[ORM\Column(
        length: 255,
        nullable: true
    )]
    private ?string $jabatanGrade = null;

    #[ORM\Column]
    private ?int $masaKerja = null;

    #[ORM\Column(
        type: Types::SIMPLE_ARRAY
    )]
    private array $roles = [];

    #[ORM\Column(
        length: 3, nullable: true
    )]
    private ?string $legacyKodeKpp = null;

    #[ORM\Column(
        length: 3, nullable: true
    )]
    private ?string $legacyKodeKanwil = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPegawaiId(): ?Uuid
    {
        return $this->pegawaiId;
    }

    public function setPegawaiId($pegawaiId): self
    {
        $this->pegawaiId = $pegawaiId;

        return $this;
    }

    public function getNamaPegawai(): ?string
    {
        return $this->namaPegawai;
    }

    public function setNamaPegawai(?string $namaPegawai): self
    {
        $this->namaPegawai = $namaPegawai;

        return $this;
    }

    public function getNip9(): ?string
    {
        return $this->nip9;
    }

    public function setNip9(?string $nip9): self
    {
        $this->nip9 = $nip9;

        return $this;
    }

    public function getNip18(): ?string
    {
        return $this->nip18;
    }

    public function setNip18(?string $nip18): self
    {
        $this->nip18 = $nip18;

        return $this;
    }

    public function getNik(): ?string
    {
        return $this->nik;
    }

    public function setNik(string $nik): self
    {
        $this->nik = $nik;

        return $this;
    }

    public function getPangkatId(): ?Uuid
    {
        return $this->pangkatId;
    }

    public function setPangkatId($pangkatId): self
    {
        $this->pangkatId = $pangkatId;

        return $this;
    }

    public function getPangkat(): ?string
    {
        return $this->pangkat;
    }

    public function setPangkat(?string $pangkat): self
    {
        $this->pangkat = $pangkat;

        return $this;
    }

    public function getJabatanId(): ?Uuid
    {
        return $this->jabatanId;
    }

    public function setJabatanId($jabatanId): self
    {
        $this->jabatanId = $jabatanId;

        return $this;
    }

    public function getJabatan(): ?string
    {
        return $this->jabatan;
    }

    public function setJabatan(?string $jabatan): self
    {
        $this->jabatan = $jabatan;

        return $this;
    }

    public function getUnitId(): ?Uuid
    {
        return $this->unitId;
    }

    public function setUnitId($unitId): self
    {
        $this->unitId = $unitId;

        return $this;
    }

    public function getUnit(): ?string
    {
        return $this->unit;
    }

    public function setUnit(?string $unit): self
    {
        $this->unit = $unit;

        return $this;
    }

    public function getUnitLegacyKode(): ?string
    {
        return $this->unitLegacyKode;
    }

    public function setUnitLegacyKode(?string $unitLegacyKode): self
    {
        $this->unitLegacyKode = $unitLegacyKode;

        return $this;
    }

    public function getKantorId(): ?Uuid
    {
        return $this->kantorId;
    }

    public function setKantorId($kantorId): self
    {
        $this->kantorId = $kantorId;

        return $this;
    }

    public function getKantor(): ?string
    {
        return $this->kantor;
    }

    public function setKantor(?string $kantor): self
    {
        $this->kantor = $kantor;

        return $this;
    }

    public function getKantorLegacyKode(): ?string
    {
        return $this->kantorLegacyKode;
    }

    public function setKantorLegacyKode(?string $kantorLegacyKode): self
    {
        $this->kantorLegacyKode = $kantorLegacyKode;

        return $this;
    }

    public function getAgamaId(): ?UuidV4
    {
        return $this->agamaId;
    }

    public function setAgamaId($agamaId): self
    {
        $this->agamaId = $agamaId;

        return $this;
    }

    public function getAgama(): ?string
    {
        return $this->agama;
    }

    public function setAgama(?string $agama): self
    {
        $this->agama = $agama;

        return $this;
    }

    public function getJenisKelaminId(): ?UuidV4
    {
        return $this->jenisKelaminId;
    }

    public function setJenisKelaminId($jenisKelaminId): self
    {
        $this->jenisKelaminId = $jenisKelaminId;

        return $this;
    }

    public function getJenisKelamin(): ?string
    {
        return $this->jenisKelamin;
    }

    public function setJenisKelamin(?string $jenisKelamin): self
    {
        $this->jenisKelamin = $jenisKelamin;

        return $this;
    }

    public function getAlamatJalan(): ?string
    {
        return $this->alamatJalan;
    }

    public function setAlamatJalan(?string $alamatJalan): self
    {
        $this->alamatJalan = $alamatJalan;

        return $this;
    }

    public function getKelurahanId(): ?UuidV4
    {
        return $this->kelurahanId;
    }

    public function setKelurahanId($kelurahanId): self
    {
        $this->kelurahanId = $kelurahanId;

        return $this;
    }

    public function getKelurahan(): ?string
    {
        return $this->kelurahan;
    }

    public function setKelurahan(?string $kelurahan): self
    {
        $this->kelurahan = $kelurahan;

        return $this;
    }

    public function getKecamatanId(): ?UuidV4
    {
        return $this->kecamatanId;
    }

    public function setKecamatanId($kecamatanId): self
    {
        $this->kecamatanId = $kecamatanId;

        return $this;
    }

    public function getKecamatan(): ?string
    {
        return $this->kecamatan;
    }

    public function setKecamatan(?string $kecamatan): self
    {
        $this->kecamatan = $kecamatan;

        return $this;
    }

    public function getKabupatenId(): ?UuidV4
    {
        return $this->kabupatenId;
    }

    public function setKabupatenId($kabupatenId): self
    {
        $this->kabupatenId = $kabupatenId;

        return $this;
    }

    public function getKabupaten(): ?string
    {
        return $this->kabupaten;
    }

    public function setKabupaten(?string $kabupaten): self
    {
        $this->kabupaten = $kabupaten;

        return $this;
    }

    public function getProvinsiId(): ?UuidV4
    {
        return $this->provinsiId;
    }

    public function setProvinsiId($provinsiId): self
    {
        $this->provinsiId = $provinsiId;

        return $this;
    }

    public function getProvinsi(): ?string
    {
        return $this->provinsi;
    }

    public function setProvinsi(?string $provinsi): self
    {
        $this->provinsi = $provinsi;

        return $this;
    }

    public function getTanggalLahir(): ?DateTimeInterface
    {
        return $this->tanggalLahir;
    }

    public function setTanggalLahir(?DateTimeInterface $tanggalLahir): self
    {
        $this->tanggalLahir = $tanggalLahir;

        return $this;
    }

    public function getTempatLahir(): ?string
    {
        return $this->tempatLahir;
    }

    public function setTempatLahir(?string $tempatLahir): self
    {
        $this->tempatLahir = $tempatLahir;

        return $this;
    }

    public function getPensiun(): ?bool
    {
        return $this->pensiun;
    }

    public function setPensiun(?bool $pensiun): self
    {
        $this->pensiun = $pensiun;

        return $this;
    }

    public function getJenjangPendidikanId(): ?Uuid
    {
        return $this->jenjangPendidikanId;
    }

    public function setJenjangPendidikanId($jenjangPendidikanId): self
    {
        $this->jenjangPendidikanId = $jenjangPendidikanId;

        return $this;
    }

    public function getJenjangPendidikan(): ?string
    {
        return $this->jenjangPendidikan;
    }

    public function setJenjangPendidikan(?string $jenjangPendidikan): self
    {
        $this->jenjangPendidikan = $jenjangPendidikan;

        return $this;
    }

    public function getJurusanId(): ?Uuid
    {
        return $this->jurusanId;
    }

    public function setJurusanId($jurusanId): self
    {
        $this->jurusanId = $jurusanId;

        return $this;
    }

    public function getJurusan(): ?string
    {
        return $this->jurusan;
    }

    public function setJurusan(?string $jurusan): self
    {
        $this->jurusan = $jurusan;

        return $this;
    }

    public function getTglIjazah(): ?DateTimeImmutable
    {
        return $this->tglIjazah;
    }

    public function setTglIjazah(?DateTimeImmutable $tglIjazah): self
    {
        $this->tglIjazah = $tglIjazah;

        return $this;
    }

    public function getParentKantorId(): ?Uuid
    {
        return $this->parentKantorId;
    }

    public function setParentKantorId($parentKantorId): self
    {
        $this->parentKantorId = $parentKantorId;

        return $this;
    }

    public function getParentKantor(): ?string
    {
        return $this->parentKantor;
    }

    public function setParentKantor(?string $parentKantor): self
    {
        $this->parentKantor = $parentKantor;

        return $this;
    }

    public function getParentUnitId(): ?Uuid
    {
        return $this->parentUnitId;
    }

    public function setParentUnitId($parentUnitId): self
    {
        $this->parentUnitId = $parentUnitId;

        return $this;
    }

    public function getParentUnit(): ?string
    {
        return $this->parentUnit;
    }

    public function setParentUnit(?string $parentUnit): self
    {
        $this->parentUnit = $parentUnit;

        return $this;
    }

    public function getKantorEselon1Id(): ?Uuid
    {
        return $this->kantorEselon1Id;
    }

    public function setKantorEselon1Id($kantorEselon1Id): self
    {
        $this->kantorEselon1Id = $kantorEselon1Id;

        return $this;
    }

    public function getKantorEselon1(): ?string
    {
        return $this->kantorEselon1;
    }

    public function setKantorEselon1(?string $kantorEselon1): self
    {
        $this->kantorEselon1 = $kantorEselon1;

        return $this;
    }

    public function getKantorEselon2Id(): ?Uuid
    {
        return $this->kantorEselon2Id;
    }

    public function setKantorEselon2Id($kantorEselon2Id): self
    {
        $this->kantorEselon2Id = $kantorEselon2Id;

        return $this;
    }

    public function getKantorEselon2(): ?string
    {
        return $this->kantorEselon2;
    }

    public function setKantorEselon2(?string $kantorEselon2): self
    {
        $this->kantorEselon2 = $kantorEselon2;

        return $this;
    }

    public function getKantorEselon3Id(): ?Uuid
    {
        return $this->kantorEselon3Id;
    }

    public function setKantorEselon3Id($kantorEselon3Id): self
    {
        $this->kantorEselon3Id = $kantorEselon3Id;

        return $this;
    }

    public function getKantorEselon3(): ?string
    {
        return $this->kantorEselon3;
    }

    public function setKantorEselon3(?string $kantorEselon3): self
    {
        $this->kantorEselon3 = $kantorEselon3;

        return $this;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getUserId(): ?Uuid
    {
        return $this->userId;
    }

    public function setUserId(Uuid $userId): self
    {
        $this->userId = $userId;

        return $this;
    }

    public function getPosisiKelompok(): ?string
    {
        return $this->posisiKelompok;
    }

    public function setPosisiKelompok(?string $posisiKelompok): self
    {
        $this->posisiKelompok = $posisiKelompok;

        return $this;
    }

    public function getGrade(): ?int
    {
        return $this->grade;
    }

    public function setGrade(?int $grade): self
    {
        $this->grade = $grade;

        return $this;
    }

    public function getJabatanGrade(): ?string
    {
        return $this->jabatanGrade;
    }

    public function setJabatanGrade(?string $jabatanGrade): self
    {
        $this->jabatanGrade = $jabatanGrade;

        return $this;
    }

    public function getMasaKerja(): ?int
    {
        return $this->masaKerja;
    }

    public function setMasaKerja(int $masaKerja): self
    {
        $this->masaKerja = $masaKerja;

        return $this;
    }

    public function getRoles(): array
    {
        return $this->roles;
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function getLegacyKodeKpp(): ?string
    {
        return $this->legacyKodeKpp;
    }

    public function setLegacyKodeKpp(?string $legacyKodeKpp): self
    {
        $this->legacyKodeKpp = $legacyKodeKpp;

        return $this;
    }

    public function getLegacyKodeKanwil(): ?string
    {
        return $this->legacyKodeKanwil;
    }

    public function setLegacyKodeKanwil(?string $legacyKodeKanwil): self
    {
        $this->legacyKodeKanwil = $legacyKodeKanwil;

        return $this;
    }
}
