<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Put;
use App\Repository\BisdmKluRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(
    repositoryClass: BisdmKluRepository::class
)]
#[ApiResource(
    operations: [
        new Get(
            security: 'is_granted("ROLE_USER")',
            securityMessage: 'Only a valid user can access this.'
        ),
        new Put(
            security: 'is_granted("ROLE_APLIKASI") or is_granted("ROLE_ADMIN") or is_granted("ROLE_UPK_PUSAT")',
            securityMessage: 'Only admin/app can add new resource to this entity type.'
        ),
        new Patch(
            security: 'is_granted("ROLE_APLIKASI") or is_granted("ROLE_ADMIN") or is_granted("ROLE_UPK_PUSAT")',
            securityMessage: 'Only admin/app can add new resource to this entity type.'
        ),
        new Delete(
            security: 'is_granted("ROLE_APLIKASI") or is_granted("ROLE_ADMIN") or is_granted("ROLE_UPK_PUSAT")',
            securityMessage: 'Only admin/app can add new resource to this entity type.'
        ),
        new GetCollection(
            security: 'is_granted("ROLE_USER")',
            securityMessage: 'Only a valid user can access this.'
        ),
        new Post(
            security: 'is_granted("ROLE_APLIKASI") or is_granted("ROLE_ADMIN") or is_granted("ROLE_UPK_PUSAT")',
            securityMessage: 'Only admin/app can add new resource to this entity type.'
        )
    ],
    order: [
        'id' => 'ASC'
    ],
    security: 'is_granted("ROLE_USER")',
    securityMessage: 'Only a valid user can access this.'
)]
#[ORM\Table(
    name: 'bisdm_klu'
)]
#[ORM\Index(
    columns: [
        'id'
    ],
    name: 'idx_bisdm_klu'
)]
#[ORM\Index(
    columns: [
        'id',
        'kd_klu',
        'kd_klu13_segmen'
    ],
    name: 'idx_klu_kode'
)]
#[ApiFilter(
    filterClass: SearchFilter::class,
    properties: [
        'id' => 'exact',
        'kdKlu' => 'ipartial',
        'kdKlu13Segmen' => 'ipartial',
        'nmKlu13Segmen' => 'ipartial',
    ]
)]

class BisdmKlu
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(
        length: 10
    )]
    private ?string $kdKlu = null;

    #[ORM\Column(
        length: 2
    )]
    private ?string $kdKlu13Segmen = null;

    #[ORM\Column(
        length: 255
    )]
    private ?string $nmKlu13Segmen = null;

    #[ORM\Column(
        type: Types::DATE_MUTABLE,
        nullable: true
    )]
    private ?\DateTimeInterface $dwStartDate = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getKdKlu(): ?string
    {
        return $this->kdKlu;
    }

    public function setKdKlu(string $kdKlu): self
    {
        $this->kdKlu = $kdKlu;

        return $this;
    }

    public function getKdKlu13Segmen(): ?string
    {
        return $this->kdKlu13Segmen;
    }

    public function setKdKlu13Segmen(string $kdKlu13Segmen): self
    {
        $this->kdKlu13Segmen = $kdKlu13Segmen;

        return $this;
    }

    public function getNmKlu13Segmen(): ?string
    {
        return $this->nmKlu13Segmen;
    }

    public function setNmKlu13Segmen(string $nmKlu13Segmen): self
    {
        $this->nmKlu13Segmen = $nmKlu13Segmen;

        return $this;
    }

    public function getDwStartDate(): ?\DateTimeInterface
    {
        return $this->dwStartDate;
    }

    public function setDwStartDate(?\DateTimeInterface $dwStartDate): self
    {
        $this->dwStartDate = $dwStartDate;

        return $this;
    }
}
