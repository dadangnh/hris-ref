<?php

namespace App\Entity\Posisi;

use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Serializer\Filter\PropertyFilter;
use ApiPlatform\Doctrine\Orm\Filter\NumericFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use App\Repository\Posisi\KecamatanRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Uid\UuidV4;

/**
 * Kecamatan Class
 */
#[ApiResource(
    operations: [
        new Get(
            security: 'is_granted("ROLE_USER")',
            securityMessage: 'Only a valid user can access this.'
        ),
        new Put(
            security: 'is_granted("ROLE_APLIKASI") or is_granted("ROLE_ADMIN") or is_granted("ROLE_UPK_PUSAT")',
            securityMessage: 'Only admin/app can add new resource to this entity type.'
        ),
        new Patch(
            security: 'is_granted("ROLE_APLIKASI") or is_granted("ROLE_ADMIN") or is_granted("ROLE_UPK_PUSAT")',
            securityMessage: 'Only admin/app can add new resource to this entity type.'
        ),
        new Delete(
            security: 'is_granted("ROLE_APLIKASI") or is_granted("ROLE_ADMIN") or is_granted("ROLE_UPK_PUSAT")',
            securityMessage: 'Only admin/app can add new resource to this entity type.'
        ),
        new GetCollection(
            security: 'is_granted("ROLE_USER")',
            securityMessage: 'Only a valid user can access this.'
        ),
        new Post(
            security: 'is_granted("ROLE_APLIKASI") or is_granted("ROLE_ADMIN") or is_granted("ROLE_UPK_PUSAT")',
            securityMessage: 'Only admin/app can add new resource to this entity type.'
        )
    ],
    order: [
        'kodeDbMaster' => 'ASC',
        'kodeKemendagri' => 'ASC'
    ],
    security: 'is_granted("ROLE_USER")',
    securityMessage: 'Only a valid user can access this.'
)]
#[ORM\Entity(
    repositoryClass: KecamatanRepository::class
)]
#[ORM\Table(
    name: 'kecamatan'
)]
#[ORM\Index(
    columns: [
        'id',
        'nama',
        'nama_administrasi'
    ],
    name: 'idx_kecamatan_nama'
)]
#[ORM\Index(
    columns: [
        'id',
        'id_db_master',
        'id_kabupaten_db_master',
        'kode_db_master',
        'kode_kemendagri'
    ],
    name: 'idx_kecamatan_legacy'
)]
#[ORM\Cache(
    usage: 'NONSTRICT_READ_WRITE'
)]
#[UniqueEntity(
    fields: [
        'idDbMaster',
        'kodeDbMaster'
    ]
)]
#[ApiFilter(
    filterClass: SearchFilter::class,
    properties: [
        'id' => 'exact',
        'nama' => 'ipartial',
        'namaAdministrasi' => 'ipartial',
        'kodeDbMaster' => 'iexact',
        'kodeKemendagri' => 'iexact',
        'kabupatenKota.id' => 'exact',
        'kabupatenKota.nama' => 'iexact',
        'kabupatenKota.namaAdministrasi' => 'iexact',
        'kabupatenKota.provinsi.id' => 'exact',
        'kabupatenKota.provinsi.nama' => 'iexact',
        'kabupatenKota.provinsi.namaAdministrasi' => 'iexact'
    ]
)]
#[ApiFilter(
    filterClass: NumericFilter::class,
    properties: [
        'idDbMaster',
        'idKabupatenDbMaster'
    ]
)]
#[ApiFilter(
    filterClass: PropertyFilter::class
)]
class Kecamatan
{
    #[ORM\Id]
    #[ORM\Column(
        type: 'uuid',
        unique: true
    )]
    #[ORM\Cache(
        usage: 'NONSTRICT_READ_WRITE'
    )]
    private UuidV4 $id;

    #[ORM\Column(
        type: Types::INTEGER
    )]
    #[ORM\Cache(
        usage: 'NONSTRICT_READ_WRITE'
    )]
    private ?int $idDbMaster;

    #[ORM\Column(
        type: Types::INTEGER
    )]
    #[ORM\Cache(
        usage: 'NONSTRICT_READ_WRITE'
    )]
    private ?int $idKabupatenDbMaster;

    #[ORM\Column(
        type: Types::STRING,
        length: 7
    )]
    #[ORM\Cache(
        usage: 'NONSTRICT_READ_WRITE'
    )]
    private ?string $kodeDbMaster;

    #[ORM\Column(
        type: Types::STRING,
        length: 20
    )]
    #[ORM\Cache(
        usage: 'NONSTRICT_READ_WRITE'
    )]
    private ?string $namaAdministrasi;

    #[ORM\Column(
        type: Types::STRING,
        length: 50
    )]
    #[ORM\Cache(
        usage: 'NONSTRICT_READ_WRITE'
    )]
    private ?string $nama;

    #[ORM\Column(
        type: Types::STRING,
        length: 10
    )]
    #[ORM\Cache(
        usage: 'NONSTRICT_READ_WRITE'
    )]
    private ?string $kodeKemendagri;

    #[ORM\ManyToOne(
        targetEntity: KabupatenKota::class,
        inversedBy: 'kecamatans'
    )]
    #[ORM\JoinColumn(
        nullable: false
    )]
    private ?KabupatenKota $kabupatenKota;

    #[ORM\OneToMany(
        mappedBy: 'kecamatan',
        targetEntity: Kelurahan::class
    )]
    private Collection $kelurahans;

    public function __construct()
    {
        $this->id = Uuid::v4();
        $this->kelurahans = new ArrayCollection();
    }

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function getIdDbMaster(): ?int
    {
        return $this->idDbMaster;
    }

    public function setIdDbMaster(int $idDbMaster): self
    {
        $this->idDbMaster = $idDbMaster;

        return $this;
    }

    public function getIdKabupatenDbMaster(): ?int
    {
        return $this->idKabupatenDbMaster;
    }

    public function setIdKabupatenDbMaster(int $idKabupatenDbMaster): self
    {
        $this->idKabupatenDbMaster = $idKabupatenDbMaster;

        return $this;
    }

    public function getKodeDbMaster(): ?string
    {
        return $this->kodeDbMaster;
    }

    public function setKodeDbMaster(string $kodeDbMaster): self
    {
        $this->kodeDbMaster = $kodeDbMaster;

        return $this;
    }

    public function getNamaAdministrasi(): ?string
    {
        return $this->namaAdministrasi;
    }

    public function setNamaAdministrasi(string $namaAdministrasi): self
    {
        $this->namaAdministrasi = $namaAdministrasi;

        return $this;
    }

    public function getNama(): ?string
    {
        return $this->nama;
    }

    public function setNama(string $nama): self
    {
        $this->nama = $nama;

        return $this;
    }

    public function getKodeKemendagri(): ?string
    {
        return $this->kodeKemendagri;
    }

    public function setKodeKemendagri(string $kodeKemendagri): self
    {
        $this->kodeKemendagri = $kodeKemendagri;

        return $this;
    }

    public function getKabupatenKota(): ?KabupatenKota
    {
        return $this->kabupatenKota;
    }

    public function setKabupatenKota(?KabupatenKota $kabupatenKota): self
    {
        $this->kabupatenKota = $kabupatenKota;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getKelurahans(): Collection
    {
        return $this->kelurahans;
    }

    public function addKelurahan(Kelurahan $kelurahan): self
    {
        if (!$this->kelurahans->contains($kelurahan)) {
            $this->kelurahans[] = $kelurahan;
            $kelurahan->setKecamatan($this);
        }

        return $this;
    }

    public function removeKelurahan(Kelurahan $kelurahan): self
    {
        if ($this->kelurahans->removeElement($kelurahan)) {
            // set the owning side to null (unless already changed)
            if ($kelurahan->getKecamatan() === $this) {
                $kelurahan->setKecamatan(null);
            }
        }

        return $this;
    }
}
