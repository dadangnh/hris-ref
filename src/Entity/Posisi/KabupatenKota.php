<?php

namespace App\Entity\Posisi;

use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Serializer\Filter\PropertyFilter;
use ApiPlatform\Doctrine\Orm\Filter\NumericFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use App\Repository\Posisi\KabupatenKotaRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Uid\UuidV4;

/**
 * Kabupaten Kota Class
 */
#[ApiResource(
    operations: [
        new Get(
            security: 'is_granted("ROLE_USER")',
            securityMessage: 'Only a valid user can access this.'
        ),
        new Put(
            security: 'is_granted("ROLE_APLIKASI") or is_granted("ROLE_ADMIN") or is_granted("ROLE_UPK_PUSAT")',
            securityMessage: 'Only admin/app can add new resource to this entity type.'
        ),
        new Patch(
            security: 'is_granted("ROLE_APLIKASI") or is_granted("ROLE_ADMIN") or is_granted("ROLE_UPK_PUSAT")',
            securityMessage: 'Only admin/app can add new resource to this entity type.'
        ),
        new Delete(
            security: 'is_granted("ROLE_APLIKASI") or is_granted("ROLE_ADMIN") or is_granted("ROLE_UPK_PUSAT")',
            securityMessage: 'Only admin/app can add new resource to this entity type.'
        ),
        new GetCollection(
            security: 'is_granted("ROLE_USER")',
            securityMessage: 'Only a valid user can access this.'
        ),
        new Post(
            security: 'is_granted("ROLE_APLIKASI") or is_granted("ROLE_ADMIN") or is_granted("ROLE_UPK_PUSAT")',
            securityMessage: 'Only admin/app can add new resource to this entity type.'
        )
    ],
    order: [
        'kodeDbMaster' => 'ASC',
        'kodeKemendagri' => 'ASC'
    ],
    security: 'is_granted("ROLE_USER")',
    securityMessage: 'Only a valid user can access this.'
)]
#[ORM\Entity(
    repositoryClass: KabupatenKotaRepository::class
)]
#[ORM\Table(
    name: 'kabupaten_kota'
)]
#[ORM\Index(
    columns: [
        'id',
        'nama',
        'nama_administrasi'
    ],
    name: 'idx_kabupaten_kota_nama'
)]
#[ORM\Index(
    columns: [
        'id',
        'id_db_master',
        'id_provinsi_db_master',
        'kode_db_master',
        'kode_kemendagri'
    ],
    name: 'idx_kabupaten_kota_legacy'
)]
#[ORM\Cache(
    usage: 'NONSTRICT_READ_WRITE'
)]
#[UniqueEntity(
    fields: [
        'idDbMaster',
        'kodeDbMaster'
    ]
)]
#[ApiFilter(
    filterClass: SearchFilter::class,
    properties: [
        'id' => 'exact',
        'nama' => 'ipartial',
        'namaAdministrasi' => 'ipartial',
        'kodeDbMaster' => 'iexact',
        'kodeKemendagri' => 'iexact',
        'provinsi.id' => 'exact',
        'provinsi.nama' => 'iexact',
        'provinsi.namaAdministrasi' => 'iexact'
    ]
)]
#[ApiFilter(
    filterClass: NumericFilter::class,
    properties: [
        'idDbMaster',
        'idProvinsiDbMaster'
    ]
)]
#[ApiFilter(
    filterClass: PropertyFilter::class
)]
class KabupatenKota
{
    #[ORM\Id]
    #[ORM\Column(
        type: 'uuid',
        unique: true
    )]
    #[ORM\Cache(
        usage: 'NONSTRICT_READ_WRITE'
    )]
    private UuidV4 $id;

    #[ORM\Column(
        type: Types::INTEGER
    )]
    #[ORM\Cache(
        usage: 'NONSTRICT_READ_WRITE'
    )]
    private ?int $idDbMaster;

    #[ORM\Column(
        type: Types::INTEGER
    )]
    #[ORM\Cache(
        usage: 'NONSTRICT_READ_WRITE'
    )]
    private ?int $IdProvinsiDbMaster;

    #[ORM\Column(
        type: Types::STRING,
        length: 4
    )]
    #[ORM\Cache(
        usage: 'NONSTRICT_READ_WRITE'
    )]
    private ?string $kodeDbMaster;

    #[ORM\Column(
        type: Types::STRING,
        length: 20
    )]
    #[ORM\Cache(
        usage: 'NONSTRICT_READ_WRITE'
    )]
    private ?string $namaAdministrasi;

    #[ORM\Column(
        type: Types::STRING,
        length: 50
    )]
    #[ORM\Cache(
        usage: 'NONSTRICT_READ_WRITE'
    )]
    private ?string $nama;

    #[ORM\Column(
        type: Types::STRING,
        length: 10
    )]
    #[ORM\Cache(
        usage: 'NONSTRICT_READ_WRITE'
    )]
    private ?string $kodeKemendagri;

    #[ORM\ManyToOne(
        targetEntity: Provinsi::class,
        inversedBy: 'kabupatenKotas'
    )]
    #[ORM\JoinColumn(
        nullable: false
    )]
    private ?Provinsi $provinsi;

    #[ORM\OneToMany(
        mappedBy: 'kabupatenKota',
        targetEntity: Kecamatan::class
    )]
    private Collection $kecamatans;

    public function __construct()
    {
        $this->id = Uuid::v4();
        $this->kecamatans = new ArrayCollection();
    }

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function getIdDbMaster(): ?int
    {
        return $this->idDbMaster;
    }

    public function setIdDbMaster(int $idDbMaster): self
    {
        $this->idDbMaster = $idDbMaster;

        return $this;
    }

    public function getIdProvinsiDbMaster(): ?int
    {
        return $this->IdProvinsiDbMaster;
    }

    public function setIdProvinsiDbMaster(int $IdProvinsiDbMaster): self
    {
        $this->IdProvinsiDbMaster = $IdProvinsiDbMaster;

        return $this;
    }

    public function getKodeDbMaster(): ?string
    {
        return $this->kodeDbMaster;
    }

    public function setKodeDbMaster(string $kodeDbMaster): self
    {
        $this->kodeDbMaster = $kodeDbMaster;

        return $this;
    }

    public function getNamaAdministrasi(): ?string
    {
        return $this->namaAdministrasi;
    }

    public function setNamaAdministrasi(string $namaAdministrasi): self
    {
        $this->namaAdministrasi = $namaAdministrasi;

        return $this;
    }

    public function getNama(): ?string
    {
        return $this->nama;
    }

    public function setNama(string $nama): self
    {
        $this->nama = $nama;

        return $this;
    }

    public function getKodeKemendagri(): ?string
    {
        return $this->kodeKemendagri;
    }

    public function setKodeKemendagri(string $kodeKemendagri): self
    {
        $this->kodeKemendagri = $kodeKemendagri;

        return $this;
    }

    public function getProvinsi(): ?Provinsi
    {
        return $this->provinsi;
    }

    public function setProvinsi(?Provinsi $provinsi): self
    {
        $this->provinsi = $provinsi;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getKecamatans(): Collection
    {
        return $this->kecamatans;
    }

    public function addKecamatan(Kecamatan $kecamatan): self
    {
        if (!$this->kecamatans->contains($kecamatan)) {
            $this->kecamatans[] = $kecamatan;
            $kecamatan->setKabupatenKota($this);
        }

        return $this;
    }

    public function removeKecamatan(Kecamatan $kecamatan): self
    {
        if ($this->kecamatans->removeElement($kecamatan)) {
            // set the owning side to null (unless already changed)
            if ($kecamatan->getKabupatenKota() === $this) {
                $kecamatan->setKabupatenKota(null);
            }
        }

        return $this;
    }
}
