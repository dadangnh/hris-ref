<?php

namespace App\Entity\Posisi;

use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Serializer\Filter\PropertyFilter;
use ApiPlatform\Doctrine\Orm\Filter\NumericFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use App\Repository\Posisi\KelurahanRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Uid\UuidV4;

/**
 * Kelurahan Class
 */
#[ApiResource(
    operations: [
        new Get(
            security: 'is_granted("ROLE_USER")',
            securityMessage: 'Only a valid user can access this.'
        ),
        new Put(
            security: 'is_granted("ROLE_APLIKASI") or is_granted("ROLE_ADMIN") or is_granted("ROLE_UPK_PUSAT")',
            securityMessage: 'Only admin/app can add new resource to this entity type.'
        ),
        new Patch(
            security: 'is_granted("ROLE_APLIKASI") or is_granted("ROLE_ADMIN") or is_granted("ROLE_UPK_PUSAT")',
            securityMessage: 'Only admin/app can add new resource to this entity type.'
        ),
        new Delete(
            security: 'is_granted("ROLE_APLIKASI") or is_granted("ROLE_ADMIN") or is_granted("ROLE_UPK_PUSAT")',
            securityMessage: 'Only admin/app can add new resource to this entity type.'
        ),
        new GetCollection(
            security: 'is_granted("ROLE_USER")',
            securityMessage: 'Only a valid user can access this.'
        ),
        new Post(
            security: 'is_granted("ROLE_APLIKASI") or is_granted("ROLE_ADMIN") or is_granted("ROLE_UPK_PUSAT")',
            securityMessage: 'Only admin/app can add new resource to this entity type.'
        )
    ],
    order: [
        'kodeDbMaster' => 'ASC',
        'kodeKemendagri' => 'ASC'
    ],
    security: 'is_granted("ROLE_USER")',
    securityMessage: 'Only a valid user can access this.'
)]
#[ORM\Entity(
    repositoryClass: KelurahanRepository::class
)]
#[ORM\Table(
    name: 'kelurahan'
)]
#[ORM\Index(
    columns: [
        'id',
        'nama',
        'nama_administrasi'
    ],
    name: 'idx_kelurahan_nama'
)]
#[ORM\Index(
    columns: [
        'id',
        'id_db_master',
        'id_kecamatan_db_master',
        'kode_db_master',
        'kode_kemendagri'
    ],
    name: 'idx_kelurahan_legacy'
)]
#[ORM\Cache(
    usage: 'NONSTRICT_READ_WRITE'
)]
#[UniqueEntity(
    fields: [
        'idDbMaster',
        'kodeDbMaster'
    ]
)]
#[ApiFilter(
    filterClass: SearchFilter::class,
    properties: [
        'id' => 'exact',
        'nama' => 'ipartial',
        'namaAdministrasi' => 'ipartial',
        'kodeDbMaster' => 'iexact',
        'kodeKemendagri' => 'iexact',
        'kecamatan.id' => 'exact',
        'kecamatan.nama' => 'iexact',
        'kecamatan.namaAdministrasi' => 'iexact',
        'kecamatan.kabupatenKota.id' => 'exact',
        'kecamatan.kabupatenKota.nama' => 'iexact',
        'kecamatan.kabupatenKota.namaAdministrasi' => 'iexact',
        'kecamatan.kabupatenKota.provinsi.id' => 'exact',
        'kecamatan.kabupatenKota.provinsi.nama' => 'iexact',
        'kecamatan.kabupatenKota.provinsi.namaAdministrasi' => 'iexact'
    ]
)]
#[ApiFilter(
    filterClass: NumericFilter::class,
    properties: [
        'idDbMaster',
        'idKecamatanDbMaster'
    ]
)]
#[ApiFilter(
    filterClass: PropertyFilter::class
)]
class Kelurahan
{
    #[ORM\Id]
    #[ORM\Column(
        type: 'uuid',
        unique: true
    )]
    #[ORM\Cache(
        usage: 'NONSTRICT_READ_WRITE'
    )]
    private UuidV4 $id;

    #[ORM\Column(
        type: Types::INTEGER
    )]
    #[ORM\Cache(
        usage: 'NONSTRICT_READ_WRITE'
    )]
    private ?int $idDbMaster;

    #[ORM\Column(
        type: Types::INTEGER
    )]
    #[ORM\Cache(
        usage: 'NONSTRICT_READ_WRITE'
    )]
    private ?int $idKecamatanDbMaster;

    #[ORM\Column(
        type: Types::STRING,
        length: 10
    )]
    #[ORM\Cache(
        usage: 'NONSTRICT_READ_WRITE'
    )]
    private ?string $kodeDbMaster;

    #[ORM\Column(
        type: Types::STRING,
        length: 20
    )]
    #[ORM\Cache(
        usage: 'NONSTRICT_READ_WRITE'
    )]
    private ?string $namaAdministrasi;

    #[ORM\Column(
        type: Types::STRING,
        length: 50
    )]
    #[ORM\Cache(
        usage: 'NONSTRICT_READ_WRITE'
    )]
    private ?string $nama;

    #[ORM\Column(
        type: Types::STRING,
        length: 5
    )]
    #[ORM\Cache(
        usage: 'NONSTRICT_READ_WRITE'
    )]
    private ?string $kodePos;

    #[ORM\Column(
        type: Types::STRING,
        length: 10
    )]
    #[ORM\Cache(
        usage: 'NONSTRICT_READ_WRITE'
    )]
    private ?string $kodeKemendagri;

    #[ORM\ManyToOne(
        targetEntity: Kecamatan::class,inversedBy: 'kelurahans'
    )]
    #[ORM\JoinColumn(
        nullable: false
    )]
    private ?Kecamatan $kecamatan;

    public function __construct()
    {
        $this->id = Uuid::v4();
    }

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function getIdDbMaster(): ?int
    {
        return $this->idDbMaster;
    }

    public function setIdDbMaster(string $idDbMaster): self
    {
        $this->idDbMaster = $idDbMaster;

        return $this;
    }

    public function getIdKecamatanDbMaster(): ?int
    {
        return $this->idKecamatanDbMaster;
    }

    public function setIdKecamatanDbMaster(int $idKecamatanDbMaster): self
    {
        $this->idKecamatanDbMaster = $idKecamatanDbMaster;

        return $this;
    }

    public function getKodeDbMaster(): ?string
    {
        return $this->kodeDbMaster;
    }

    public function setKodeDbMaster(string $kodeDbMaster): self
    {
        $this->kodeDbMaster = $kodeDbMaster;

        return $this;
    }

    public function getNamaAdministrasi(): ?string
    {
        return $this->namaAdministrasi;
    }

    public function setNamaAdministrasi(string $namaAdministrasi): self
    {
        $this->namaAdministrasi = $namaAdministrasi;

        return $this;
    }

    public function getNama(): ?string
    {
        return $this->nama;
    }

    public function setNama(string $nama): self
    {
        $this->nama = $nama;

        return $this;
    }

    public function getKodePos(): ?string
    {
        return $this->kodePos;
    }

    public function setKodePos(string $kodePos): self
    {
        $this->kodePos = $kodePos;

        return $this;
    }

    public function getKodeKemendagri(): ?string
    {
        return $this->kodeKemendagri;
    }

    public function setKodeKemendagri(string $kodeKemendagri): self
    {
        $this->kodeKemendagri = $kodeKemendagri;

        return $this;
    }

    public function getKecamatan(): ?Kecamatan
    {
        return $this->kecamatan;
    }

    public function setKecamatan(?Kecamatan $kecamatan): self
    {
        $this->kecamatan = $kecamatan;

        return $this;
    }
}
