# HRIS-REF (Backend for common reference for DJP HRIS)

[![pipeline status](https://gitlab.com/dadangnh/hris-ref/badges/master/pipeline.svg)](https://gitlab.com/dadangnh/hris-ref/-/commits/master)
[![Codacy Badge](https://app.codacy.com/project/badge/Grade/4e30d3c0b9f74f6c8314fe1a0eea70ae)](https://www.codacy.com/gl/dadangnh/hris-ref/dashboard?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=dadangnh/hris-ref&amp;utm_campaign=Badge_Grade)

Source code Backend of DJP HRIS Common Reference.

## Canonical source

The canonical source of DJP IAM where all development takes place is [hosted on GitLab.com](https://gitlab.com/dadangnh/hris-ref).

## Installation

Please check [INSTALLATION.md](INSTALLATION.md) for the installation instruction.

## Contributing

This is an open source project, and we are very happy to accept community contributions.

# License

This code is published under [GPLv3 License](LICENSE).
