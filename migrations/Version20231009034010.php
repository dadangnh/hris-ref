<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231009034010 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE list_pegawai (id UUID NOT NULL, pegawai_id UUID NOT NULL, nama_pegawai VARCHAR(255) NOT NULL, nip9 VARCHAR(9) DEFAULT NULL, nip18 VARCHAR(18) DEFAULT NULL, nik VARCHAR(16) DEFAULT NULL, pangkat_id UUID NOT NULL, pangkat VARCHAR(255) NOT NULL, jabatan_id UUID NOT NULL, jabatan VARCHAR(255) NOT NULL, unit_id UUID DEFAULT NULL, unit VARCHAR(255) DEFAULT NULL, kantor_id UUID NOT NULL, kantor VARCHAR(255) NOT NULL, agama_id UUID NOT NULL, agama VARCHAR(255) NOT NULL, jenis_kelamin_id UUID NOT NULL, jenis_kelamin VARCHAR(255) NOT NULL, tanggal_lahir DATE DEFAULT NULL, tempat_lahir VARCHAR(255) DEFAULT NULL, pensiun BOOLEAN NOT NULL, unit_legacy_kode VARCHAR(10) DEFAULT NULL, kantor_legacy_kode VARCHAR(10) DEFAULT NULL, jenjang_pendidikan_id UUID DEFAULT NULL, jenjang_pendidikan VARCHAR(255) DEFAULT NULL, parent_kantor_id UUID DEFAULT NULL, parent_kantor VARCHAR(255) DEFAULT NULL, parent_unit_id UUID DEFAULT NULL, parent_unit VARCHAR(255) DEFAULT NULL, kantor_eselon1_id UUID DEFAULT NULL, kantor_eselon1 VARCHAR(255) DEFAULT NULL, kantor_eselon2_id UUID DEFAULT NULL, kantor_eselon2 VARCHAR(255) DEFAULT NULL, kantor_eselon3_id UUID DEFAULT NULL, kantor_eselon3 VARCHAR(255) DEFAULT NULL, username VARCHAR(255) NOT NULL, user_id UUID NOT NULL, posisi_kelompok VARCHAR(255) DEFAULT NULL, grade INT DEFAULT NULL, jabatan_grade VARCHAR(255) DEFAULT NULL, masa_kerja INT DEFAULT NULL, roles TEXT NOT NULL, legacy_kode_kpp VARCHAR(3) NOT NULL, legacy_kode_kanwil VARCHAR(3) NOT NULL, tipe_jabatan VARCHAR(50) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_list_pegawaiid ON list_pegawai (id, pegawai_id)');
        $this->addSql('CREATE INDEX idx_list_identityx ON list_pegawai (id, nip18, nik)');
        $this->addSql('CREATE INDEX idx_list_search ON list_pegawai (id, nip18, nama_pegawai, jabatan, unit, kantor, masa_kerja, grade, jabatan_grade, roles)');
        $this->addSql('CREATE INDEX idx_list_legacy ON list_pegawai (id, unit, unit_id, unit_legacy_kode, kantor, kantor_id, kantor_legacy_kode)');
        $this->addSql('CREATE INDEX idx_list_user ON list_pegawai (id, user_id, username, pegawai_id, nip18, nik)');
        $this->addSql('COMMENT ON COLUMN list_pegawai.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN list_pegawai.pegawai_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN list_pegawai.pangkat_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN list_pegawai.jabatan_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN list_pegawai.unit_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN list_pegawai.kantor_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN list_pegawai.agama_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN list_pegawai.jenis_kelamin_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN list_pegawai.jenjang_pendidikan_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN list_pegawai.parent_kantor_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN list_pegawai.parent_unit_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN list_pegawai.kantor_eselon1_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN list_pegawai.kantor_eselon2_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN list_pegawai.kantor_eselon3_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN list_pegawai.user_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN list_pegawai.roles IS \'(DC2Type:simple_array)\'');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP TABLE list_pegawai');
    }
}
