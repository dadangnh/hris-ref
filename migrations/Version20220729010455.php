<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220729010455 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX idx_master_search');
        $this->addSql('CREATE INDEX idx_master_user ON master_pegawai (id, user_id, username, pegawai_id, nip9, nip18, nik)');
        $this->addSql('CREATE INDEX idx_master_search ON master_pegawai (id, nama_pegawai, jabatan, unit, kantor, jenjang_pendidikan_id, jenjang_pendidikan, jurusan, masa_kerja, grade, jabatan_grade, posisi_kelompok)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP INDEX idx_master_user');
        $this->addSql('DROP INDEX idx_master_search');
        $this->addSql('CREATE INDEX idx_master_search ON master_pegawai (id, nama_pegawai, jabatan, unit, kantor, jenjang_pendidikan_id, jenjang_pendidikan, jurusan)');
    }
}
