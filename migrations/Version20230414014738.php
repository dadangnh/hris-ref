<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230414014738 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE master_pegawai ADD legacy_kode_kpp VARCHAR(3) DEFAULT NULL');
        $this->addSql('ALTER TABLE master_pegawai ADD legacy_kode_kanwil VARCHAR(3) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE master_pegawai DROP legacy_kode_kpp');
        $this->addSql('ALTER TABLE master_pegawai DROP legacy_kode_kanwil');
    }
}
