<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230412073638 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE bisdm_klasifikasi_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE bisdm_klu_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE bisdm_ranking_per_klu_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE bisdm_klasifikasi (id INT NOT NULL, nip9 VARCHAR(9) NOT NULL, kd_jab_struktural VARCHAR(3) DEFAULT NULL, kd_jab_fungsional VARCHAR(4) DEFAULT NULL, maturity_to_work DOUBLE PRECISION DEFAULT NULL, kelas VARCHAR(50) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_bisdm_klasifikasi ON bisdm_klasifikasi (id)');
        $this->addSql('CREATE INDEX idx_klasifikasi_kode ON bisdm_klasifikasi (id, nip9)');
        $this->addSql('CREATE INDEX idx_klasifikasi_search ON bisdm_klasifikasi (id, nip9, kelas)');
        $this->addSql('CREATE TABLE bisdm_klu (id INT NOT NULL, kd_klu VARCHAR(10) NOT NULL, kd_klu13_segmen VARCHAR(2) NOT NULL, nm_klu13_segmen VARCHAR(255) NOT NULL, dw_start_date DATE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_bisdm_klu ON bisdm_klu (id)');
        $this->addSql('CREATE INDEX idx_klu_kode ON bisdm_klu (id, kd_klu, kd_klu13_segmen)');
        $this->addSql('CREATE TABLE bisdm_ranking_per_klu (id INT NOT NULL, nip9 VARCHAR(9) NOT NULL, kd_klu13_segmen VARCHAR(2) NOT NULL, ranking INT NOT NULL, dw_start_date DATE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_bisdm_ranking ON bisdm_ranking_per_klu (id)');
        $this->addSql('CREATE INDEX idx_ranking_kode ON bisdm_ranking_per_klu (id, nip9, kd_klu13_segmen)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE bisdm_klasifikasi_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE bisdm_klu_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE bisdm_ranking_per_klu_id_seq CASCADE');
        $this->addSql('DROP TABLE bisdm_klasifikasi');
        $this->addSql('DROP TABLE bisdm_klu');
        $this->addSql('DROP TABLE bisdm_ranking_per_klu');
    }
}
