<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220404053135 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX idx_master_search');
        $this->addSql('ALTER TABLE master_pegawai ADD jenjang_pendidikan_id UUID DEFAULT NULL');
        $this->addSql('ALTER TABLE master_pegawai ADD jenjang_pendidikan VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE master_pegawai ADD jurusan_id UUID DEFAULT NULL');
        $this->addSql('ALTER TABLE master_pegawai ADD jurusan VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE master_pegawai ADD tgl_ijazah TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE master_pegawai ADD parent_kantor_id UUID DEFAULT NULL');
        $this->addSql('ALTER TABLE master_pegawai ADD parent_kantor VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE master_pegawai ADD parent_unit_id UUID DEFAULT NULL');
        $this->addSql('ALTER TABLE master_pegawai ADD parent_unit VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE master_pegawai ADD kantor_eselon1_id UUID DEFAULT NULL');
        $this->addSql('ALTER TABLE master_pegawai ADD kantor_eselon1 VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE master_pegawai ADD kantor_eselon2_id UUID DEFAULT NULL');
        $this->addSql('ALTER TABLE master_pegawai ADD kantor_eselon2 VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE master_pegawai ADD kantor_eselon3_id UUID DEFAULT NULL');
        $this->addSql('ALTER TABLE master_pegawai ADD kantor_eselon3 VARCHAR(255) DEFAULT NULL');
        $this->addSql('COMMENT ON COLUMN master_pegawai.jenjang_pendidikan_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN master_pegawai.jurusan_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN master_pegawai.tgl_ijazah IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN master_pegawai.parent_kantor_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN master_pegawai.parent_unit_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN master_pegawai.kantor_eselon1_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN master_pegawai.kantor_eselon2_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN master_pegawai.kantor_eselon3_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE INDEX IDX_44A4BA20BF396750DCBB0C53F8BD700D676C949A3CC27032C104734A43F ON master_pegawai (id, unit, unit_id, unit_legacy_kode, kantor, kantor_id, kantor_legacy_kode, parent_kantor_id, parent_kantor, parent_unit_id, parent_unit, kantor_eselon1_id, kantor_eselon1, kantor_eselon2_id, kantor_eselon2, kantor_eselon3_id, kantor_eselon3)');
        $this->addSql('CREATE INDEX idx_master_search ON master_pegawai (id, nama_pegawai, jabatan, unit, kantor, jenjang_pendidikan_id, jenjang_pendidikan, jurusan)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP INDEX IDX_44A4BA20BF396750DCBB0C53F8BD700D676C949A3CC27032C104734A43F');
        $this->addSql('DROP INDEX idx_master_search');
        $this->addSql('ALTER TABLE master_pegawai DROP jenjang_pendidikan_id');
        $this->addSql('ALTER TABLE master_pegawai DROP jenjang_pendidikan');
        $this->addSql('ALTER TABLE master_pegawai DROP jurusan_id');
        $this->addSql('ALTER TABLE master_pegawai DROP jurusan');
        $this->addSql('ALTER TABLE master_pegawai DROP tgl_ijazah');
        $this->addSql('ALTER TABLE master_pegawai DROP parent_kantor_id');
        $this->addSql('ALTER TABLE master_pegawai DROP parent_kantor');
        $this->addSql('ALTER TABLE master_pegawai DROP parent_unit_id');
        $this->addSql('ALTER TABLE master_pegawai DROP parent_unit');
        $this->addSql('ALTER TABLE master_pegawai DROP kantor_eselon1_id');
        $this->addSql('ALTER TABLE master_pegawai DROP kantor_eselon1');
        $this->addSql('ALTER TABLE master_pegawai DROP kantor_eselon2_id');
        $this->addSql('ALTER TABLE master_pegawai DROP kantor_eselon2');
        $this->addSql('ALTER TABLE master_pegawai DROP kantor_eselon3_id');
        $this->addSql('ALTER TABLE master_pegawai DROP kantor_eselon3');
        $this->addSql('CREATE INDEX idx_master_search ON master_pegawai (id, nama_pegawai, jabatan, unit, kantor)');
    }
}
