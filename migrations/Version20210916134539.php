<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210916134539 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE master_pegawai_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE agama (id UUID NOT NULL, nama VARCHAR(255) NOT NULL, legacy_kode INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_agama_nama ON agama (id, nama)');
        $this->addSql('CREATE INDEX idx_agama_legacy ON agama (id, legacy_kode)');
        $this->addSql('COMMENT ON COLUMN agama.id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE golongan (id UUID NOT NULL, nama VARCHAR(5) NOT NULL, legacy_kode INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_golongan_nama ON golongan (id, nama)');
        $this->addSql('CREATE INDEX idx_golongan_legacy ON golongan (id, legacy_kode)');
        $this->addSql('COMMENT ON COLUMN golongan.id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE jenis_kelamin (id UUID NOT NULL, nama VARCHAR(255) NOT NULL, legacy_kode INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_jenis_kelamin_nama ON jenis_kelamin (id, nama)');
        $this->addSql('CREATE INDEX idx_jenis_kelamin_legacy ON jenis_kelamin (id, legacy_kode)');
        $this->addSql('COMMENT ON COLUMN jenis_kelamin.id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE jenis_sk (id UUID NOT NULL, jns_sk VARCHAR(2) NOT NULL, ket_sk VARCHAR(100) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_jenissk_jnssk ON jenis_sk (id, jns_sk)');
        $this->addSql('CREATE INDEX idx_jenissk_ket ON jenis_sk (id, ket_sk)');
        $this->addSql('COMMENT ON COLUMN jenis_sk.id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE kabupaten_kota (id UUID NOT NULL, provinsi_id UUID NOT NULL, id_db_master INT NOT NULL, id_provinsi_db_master INT NOT NULL, kode_db_master VARCHAR(4) NOT NULL, nama_administrasi VARCHAR(20) NOT NULL, nama VARCHAR(50) NOT NULL, kode_kemendagri VARCHAR(10) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_901509BAC3703970 ON kabupaten_kota (provinsi_id)');
        $this->addSql('CREATE INDEX idx_kabupaten_kota_nama ON kabupaten_kota (id, nama, nama_administrasi)');
        $this->addSql('CREATE INDEX idx_kabupaten_kota_legacy ON kabupaten_kota (id, id_db_master, id_provinsi_db_master, kode_db_master, kode_kemendagri)');
        $this->addSql('COMMENT ON COLUMN kabupaten_kota.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN kabupaten_kota.provinsi_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE kecamatan (id UUID NOT NULL, kabupaten_kota_id UUID NOT NULL, id_db_master INT NOT NULL, id_kabupaten_db_master INT NOT NULL, kode_db_master VARCHAR(7) NOT NULL, nama_administrasi VARCHAR(20) NOT NULL, nama VARCHAR(50) NOT NULL, kode_kemendagri VARCHAR(10) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_BEEEDA201297E1C4 ON kecamatan (kabupaten_kota_id)');
        $this->addSql('CREATE INDEX idx_kecamatan_nama ON kecamatan (id, nama, nama_administrasi)');
        $this->addSql('CREATE INDEX idx_kecamatan_legacy ON kecamatan (id, id_db_master, id_kabupaten_db_master, kode_db_master, kode_kemendagri)');
        $this->addSql('COMMENT ON COLUMN kecamatan.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN kecamatan.kabupaten_kota_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE kelurahan (id UUID NOT NULL, kecamatan_id UUID NOT NULL, id_db_master INT NOT NULL, id_kecamatan_db_master INT NOT NULL, kode_db_master VARCHAR(10) NOT NULL, nama_administrasi VARCHAR(20) NOT NULL, nama VARCHAR(50) NOT NULL, kode_pos VARCHAR(5) NOT NULL, kode_kemendagri VARCHAR(10) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_276DB00362696184 ON kelurahan (kecamatan_id)');
        $this->addSql('CREATE INDEX idx_kelurahan_nama ON kelurahan (id, nama, nama_administrasi)');
        $this->addSql('CREATE INDEX idx_kelurahan_legacy ON kelurahan (id, id_db_master, id_kecamatan_db_master, kode_db_master, kode_kemendagri)');
        $this->addSql('COMMENT ON COLUMN kelurahan.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN kelurahan.kecamatan_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE master_pegawai (id INT NOT NULL, pegawai_id UUID DEFAULT NULL, nama_pegawai VARCHAR(255) DEFAULT NULL, nip9 VARCHAR(9) DEFAULT NULL, nip18 VARCHAR(18) DEFAULT NULL, nik VARCHAR(16) DEFAULT NULL, pangkat_id UUID DEFAULT NULL, pangkat VARCHAR(255) DEFAULT NULL, jabatan_id UUID DEFAULT NULL, jabatan VARCHAR(255) DEFAULT NULL, unit_id UUID DEFAULT NULL, unit VARCHAR(255) DEFAULT NULL, kantor_id UUID DEFAULT NULL, kantor VARCHAR(255) DEFAULT NULL, agama_id UUID DEFAULT NULL, agama VARCHAR(255) DEFAULT NULL, jenis_kelamin_id UUID DEFAULT NULL, jenis_kelamin VARCHAR(255) DEFAULT NULL, alamat_jalan VARCHAR(255) DEFAULT NULL, kelurahan_id UUID DEFAULT NULL, kelurahan VARCHAR(255) DEFAULT NULL, kecamatan_id UUID DEFAULT NULL, kecamatan VARCHAR(255) DEFAULT NULL, kabupaten_id UUID DEFAULT NULL, kabupaten VARCHAR(255) DEFAULT NULL, provinsi_id UUID DEFAULT NULL, provinsi VARCHAR(255) DEFAULT NULL, tanggal_lahir DATE DEFAULT NULL, tempat_lahir VARCHAR(255) DEFAULT NULL, pensiun BOOLEAN DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_master_pegawaiid ON master_pegawai (id, pegawai_id)');
        $this->addSql('CREATE INDEX idx_master_identityx ON master_pegawai (id, nip9, nip18, nik)');
        $this->addSql('CREATE INDEX idx_master_search ON master_pegawai (id, nama_pegawai, jabatan, unit, kantor)');
        $this->addSql('COMMENT ON COLUMN master_pegawai.pegawai_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN master_pegawai.pangkat_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN master_pegawai.jabatan_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN master_pegawai.unit_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN master_pegawai.kantor_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN master_pegawai.agama_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN master_pegawai.jenis_kelamin_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN master_pegawai.kelurahan_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN master_pegawai.kecamatan_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN master_pegawai.kabupaten_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN master_pegawai.provinsi_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE pangkat (id UUID NOT NULL, golongan_id UUID NOT NULL, nama VARCHAR(50) NOT NULL, legacy_kode INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_5D531E3EA9CB67B8 ON pangkat (golongan_id)');
        $this->addSql('CREATE INDEX idx_pangkat_nama ON pangkat (id, nama)');
        $this->addSql('CREATE INDEX idx_pangkat_legacy ON pangkat (id, legacy_kode)');
        $this->addSql('COMMENT ON COLUMN pangkat.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN pangkat.golongan_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE provinsi (id UUID NOT NULL, id_db_master INT NOT NULL, kode_db_master VARCHAR(2) NOT NULL, nama_administrasi VARCHAR(20) NOT NULL, nama VARCHAR(50) NOT NULL, kode_kemendagri VARCHAR(10) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_provinsi_nama ON provinsi (id, nama, nama_administrasi)');
        $this->addSql('CREATE INDEX idx_provinsi_legacy ON provinsi (id, id_db_master, kode_db_master, kode_kemendagri)');
        $this->addSql('COMMENT ON COLUMN provinsi.id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE kabupaten_kota ADD CONSTRAINT FK_901509BAC3703970 FOREIGN KEY (provinsi_id) REFERENCES provinsi (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE kecamatan ADD CONSTRAINT FK_BEEEDA201297E1C4 FOREIGN KEY (kabupaten_kota_id) REFERENCES kabupaten_kota (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE kelurahan ADD CONSTRAINT FK_276DB00362696184 FOREIGN KEY (kecamatan_id) REFERENCES kecamatan (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE pangkat ADD CONSTRAINT FK_5D531E3EA9CB67B8 FOREIGN KEY (golongan_id) REFERENCES golongan (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE pangkat DROP CONSTRAINT FK_5D531E3EA9CB67B8');
        $this->addSql('ALTER TABLE kecamatan DROP CONSTRAINT FK_BEEEDA201297E1C4');
        $this->addSql('ALTER TABLE kelurahan DROP CONSTRAINT FK_276DB00362696184');
        $this->addSql('ALTER TABLE kabupaten_kota DROP CONSTRAINT FK_901509BAC3703970');
        $this->addSql('DROP SEQUENCE master_pegawai_id_seq CASCADE');
        $this->addSql('DROP TABLE agama');
        $this->addSql('DROP TABLE golongan');
        $this->addSql('DROP TABLE jenis_kelamin');
        $this->addSql('DROP TABLE jenis_sk');
        $this->addSql('DROP TABLE kabupaten_kota');
        $this->addSql('DROP TABLE kecamatan');
        $this->addSql('DROP TABLE kelurahan');
        $this->addSql('DROP TABLE master_pegawai');
        $this->addSql('DROP TABLE pangkat');
        $this->addSql('DROP TABLE provinsi');
    }
}
