<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220728114025 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE master_pegawai ADD username VARCHAR(255) DEFAULT NULL');
        $this->addSql('UPDATE master_pegawai SET username = md5(nip9)');
        $this->addSql('ALTER TABLE master_pegawai ALTER COLUMN username SET NOT NULL');
        $this->addSql('ALTER TABLE master_pegawai ADD user_id UUID DEFAULT NULL');
        $this->addSql('UPDATE master_pegawai SET user_id = gen_random_uuid()');
        $this->addSql('ALTER TABLE master_pegawai ALTER COLUMN user_id SET NOT NULL');
        $this->addSql('ALTER TABLE master_pegawai ADD posisi_kelompok VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE master_pegawai ADD grade INT DEFAULT NULL');
        $this->addSql('ALTER TABLE master_pegawai ADD jabatan_grade VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE master_pegawai ADD masa_kerja INT DEFAULT NULL');
        $this->addSql('UPDATE master_pegawai SET masa_kerja = trunc(random() * 35 + 1)::int');
        $this->addSql('ALTER TABLE master_pegawai ALTER COLUMN masa_kerja SET NOT NULL');
        $this->addSql('COMMENT ON COLUMN master_pegawai.user_id IS \'(DC2Type:uuid)\'');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE master_pegawai DROP username');
        $this->addSql('ALTER TABLE master_pegawai DROP user_id');
        $this->addSql('ALTER TABLE master_pegawai DROP posisi_kelompok');
        $this->addSql('ALTER TABLE master_pegawai DROP grade');
        $this->addSql('ALTER TABLE master_pegawai DROP jabatan_grade');
        $this->addSql('ALTER TABLE master_pegawai DROP masa_kerja');
    }
}
