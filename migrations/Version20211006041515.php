<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211006041515 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE master_pegawai ADD unit_legacy_kode VARCHAR(10) DEFAULT NULL');
        $this->addSql('ALTER TABLE master_pegawai ADD kantor_legacy_kode VARCHAR(10) DEFAULT NULL');
        $this->addSql('CREATE INDEX idx_master_legacy ON master_pegawai (id, unit, unit_id, unit_legacy_kode, kantor, kantor_id, kantor_legacy_kode)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP INDEX idx_master_legacy');
        $this->addSql('ALTER TABLE master_pegawai DROP unit_legacy_kode');
        $this->addSql('ALTER TABLE master_pegawai DROP kantor_legacy_kode');
    }
}
