# HRIS-Ref Changelog

## Version 3.2.4 (current stable)
* bug fix in config api platform

## Version 3.2.3 
* bug fix in entity listPegawai order by tipe jabatan

## Version 3.2.2 
* Bug fix config packages api platform

## Version 3.2.1
* Bug fix in package api platform

## Version 3.2.0 
* Bug fix in swagger UI
* Updated Symfony Recipes
* Update Dependencies

## Version 3.1.0
* Upgrade Major Symfony Components to v7 (v7.1.0)
* Upgrade Api Platform to v4
* Updated Symfony Recipes
* Update Dependencies

## Version 3.0.0
  * Upgrade Major Symfony Components to v7 (v7.0.2)
  * Updated Symfony Recipes
  * Update Dependencies

## Version 2.3.7
  * Penyesuaian Response Endpoint get_list_peawai (#57)
  * Add Endpoint ListPegawai
  * Delete Endpoint PegawaiAdhoc, PegawaiPlh, Pegawai Luar

## Version 2.3.6
  * Add 204 status code when return empty result (#56)
  * Upgrade Dependency Symfony/Framework to v6.3.5

## Version 2.3.5
  * Change Rule For Grade Attribute on Master Pegawai Repository (#55)
  * Upgrade Dependency Symfony, Doctrine, Api-Platfrom, Lexic, dll

## Version 2.3.4
  * Change Rule For Grade Attribute on Master Pegawai Repository (#53)
  * Upgrade Symfony to v6.3 (#54)
  * Delete filter set_max_result on Master Pegawai Repository

## Version 2.3.3
  * Pembuatan Endpoint Untuk Data BISDM (#50)
  * Penyesuaian file migration 20230301031820
  * Penambahan Field legacyKodeKpp ke Entity MasterPegawai
  * Pembuatan Endpoint get_list_pegawai

## Version 2.3.2
  * Update Symfony to v6.2.6 (#49) 
  * Api Platform v3.1.2 and Other Composer Dependency
  * Add Role attribute on Master Pegawai Entity (#50)

## Version 2.3.1
  * Use PostgreSQL 15 (#47)
  * Fix GitHub Action problem (#48)
  * Remove node from test pipeline
  * Updated Symfony components and dependencies

## Version 2.3.0
  * Update Symfony to v6.2 (#43)
  * Update Api Platform to v3 (#44)
  * Update PHP to v8.2 (#45)
  * Use Doctrine Types on Entity (#46)
  * Updated Symfony recipes

## Version 2.2.1
  * Fix Update Symfony to v6.1.5 and update Twig (#39)
  * Fix Caddy failed to build (#40)
  * Fix Update Symfony to v6.1.8 and update dependencies (#41)
  * Fix Update infrastructure configuration from upstream (#42)
  * Updated Symfony components and dependencies
  * Updated Symfony recipes

## Version 2.2.0
  * Update docker and infrastructure configuration (#38)
  * Updated Symfony Components and dependencies
  * Updated Symfony recipes

## Version 2.1.2
  * Fix test job failed (#35)
  * Fix test cli failed (#36)
  * Use new docker compose naming standard (#37)
  * Updated Symfony components and dependencies

## Version 2.1.1
  * Updated Symfony components and dependencies
  * Upgrade Symfony Recipes

## Version 2.1.0
  * Upgrade Symfony Components to v6.1 (#33)
  * Updated Symfony components and dependencies
  * Added more attributes on master pegawais (#34)

## Version 2.0.6
  * Add support for private GitHub Actions Runner (#32)
  * Updated Symfony components and dependencies

## Version 2.0.5
  * Updated Symfony components and dependencies

## Version 2.0.4
  * Update upstream auth library (#30)
  * Fix GitHub action test (#31)
  * Updated Symfony components and dependencies

## Version 2.0.3
  * Add API-Doc for the new endpoint (#29)
  * Updated dependencies

## Version 2.0.2
  * Update docker-entrypoint from upstream (#26)
  * Add more endpoint (#27)
  * Add more attribute on Master Pegawai Entity (#28)
  * Add item per page configuration
  * Updated Symfony components, Symfony recipes, and dependencies

## Version 2.0.1
  * Fix regression (#25)
  * Updated Symfony components, Symfony recipes, and dependencies

## Version 2.0.0
  * Upgrade Symfony Components to Major Version v6.0 (#23)
  * Updated Symfony components, Symfony recipes, and dependencies
  * Expose more filters on endpoint

## Version 1.3.1
  * Upgrade symfony components and dependencies

## Version 1.3.0
  * Upgrade Symfony Components to Major Version v5.4 (#22)
  * Updated components and dependencies
  * Updated GitHub Action Workflow
  * Fix Gitlab CI to push all images to Docker Hub
  * Upgrade infrastructure (#24)

## Version 1.2.5
  * Upgrade Dockerfile and Gitlab CI config (#21)
  * Upgrade symfony components and dependencies

## Version 1.2.4
  * Upgrade symfony components and dependencies

## Version 1.2.3
  * Fixes masterPegawai endpoint (#19)
  * Update Gitlab CI Pipeline
  * Upgrade symfony components and dependencies

## Version 1.2.2
  * Add more filter on entity (#15)
  * Upgrade Symfony to v5.3.10 (#16)
  * Use PHP 8 Attributes on Doctrine Annotations (#17)
  * Upgrade symfony components and dependencies
  * Update Gitlab CI Pipeline

## Version 1.2.1
  * Upgrade symfony components and dependencies
  * Update SharedAuthLibrary (#14)

## Version 1.2.0
  * Add new attributes on entity (#13)
  * Components and dependencies upgrade

## Version 1.1.3
  * Upgrade PostgreSQL to v.14
  * Upgrade infrastructure config
  * Components and dependencies upgrade

## Version 1.1.2
  * Symfony recipes upgrade
  * Deprecation free fixes (#12)

## Version 1.1.1
  * Components and dependencies upgrade
  * Standardize the endpoint name - path and response status code (#11)

## Version 1.1.0
  * Components and dependencies upgrade
  * Remove Redis Bundle (#9)
  * Upgrade SharedAuth Library (#10)

## Version 1.0.0
  First release. Contains all working code for initial HRIS-Ref workload
