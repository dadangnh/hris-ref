# DJP HRIS-Ref Installation Method

To install/ deploy this service, we support the following method:

1. [Fully Dockerized for Development](#1-fully-dockerized-for-development)
2. [Fully Dockerized for Deployment and Production](#2-fully-dockerized-for-deployment-and-production)
3. [Use Symfony console](#3-use-symfony-console)
4. [Fully use native OS services](#4-fully-use-native-os-services)

## 1. Fully Dockerized For Development
### Requirement
This method only require you to have [Docker Engine](https://docker.com) installed on the host.

### Installation
First, clone this repository:

```bash
$ git clone git@gitlab.com:dadangnh/hris-ref.git some_dir
$ cd some_dir
```

Then, create your environment by editing `.env` and save as `.env.local` or you can use OS's environment variable or use [Symfony Secrets](https://symfony.com/doc/current/configuration/secrets.html). Create your JWT passphrase on the JWT_PASSPHRASE key.
Make sure to adjust the credentials on the environment for the Docker. You can find inside docker-compose.yaml file

Create the docker environment:
```bash
$ docker-compose up -d
```

TODO: Place IAM Public Key

#### Install dependency

```bash
$ docker-compose exec php composer install
```


#### Fresh Install
For new installation, do the following, existing installation can proceed to migration.

```bash
$  docker-compose exec php bin/console doctrine:database:drop --force
$  docker-compose exec php bin/console doctrine:database:create
$  docker-compose exec php bin/console make:migration
$  docker-compose exec php bin/console doctrine:migrations:migrate --no-interaction
```

Prepopulate the database with default content:
```bash
$  docker-compose exec php bin/console doctrine:fixtures:load --no-interaction
```

#### Migration
For existing database, do migration check and make migration if needed. if you have installed this before, you can make migration from previous release:
```bash
$  docker-compose exec php bin/console make:migration
```

Lastly, run the migration:
```bash
$  docker-compose exec php bin/console doctrine:migrations:migrate --no-interaction
```

Now your app are ready to use:

Landing page: [https://localhost/](https://localhost/)

API Endpoint and Docs: [https://localhost/api](https://localhost/api)

Admin page: [https://localhost/admin](https://localhost/admin)

default credentials:
```bash
root:toor
admin:admin
upk_pusat:upk_pusat
```

#### Test

Unit testing also available with the following command:

```bash
$ docker-compose exec php bin/phpunit
```

## 2. Fully Dockerized for Deployment and Production
### Requirement
This method only require you to have [Docker Engine](https://docker.com) installed on the host.

### Installation
Copy your project on the server using `git clone`, `scp` or any other tool that may fit your need.
If you use GitHub, you may want to use [a deploy key](https://docs.github.com/en/free-pro-team@latest/developers/overview/managing-deploy-keys#deploy-keys).
Deploy keys are also [supported by GitLab](https://docs.gitlab.com/ee/user/project/deploy_keys/).

Example with Git:

```bash
$ git clone git@gitlab.com:dadangnh/hris-ref.git
```

Go into the directory containing your project (`<project-name>`), and start the app in production mode:

```bash
$ SERVER_NAME=your-domain-name.example.com docker-compose -f docker-compose.yml -f docker-compose.prod.yml up -d
```


Be sure to replace `your-domain-name.example.com` by your actual domain name.

Your server is up and running, and a Let's Encrypt HTTPS certificate has been automatically generated for you.
Go to `https://your-domain-name.example.com` and enjoy!

### Disabling HTTPS

Alternatively, if you don't want to expose an HTTPS server but only an HTTP one, run the following command:

```bash
$ SERVER_NAME=:80 docker-compose -f docker-compose.yml -f docker-compose.prod.yml up -d
```

## 3. Use Symfony console
### Requirement
This method only require you to have the following tools:

1. [Docker Engine](https://docker.com) installed on the host.

2. [PHP Engine version 8.0.1 or newer](https://www.php.net) 

3. [Symfony console](https://symfony.com/download)

### Installation
First, clone this repository:

```bash
$ git clone git@gitlab.com:dadangnh/hris-ref.git some_dir
$ cd some_dir
```

Then, create your environment by editing `.env` and save as `.env.local` or you can use OS's environment variable or use [Symfony Secrets](https://symfony.com/doc/current/configuration/secrets.html). Create your JWT passphrase on the JWT_PASSPHRASE key.
Make sure to adjust the credentials on the environment for the Docker. You can find inside docker-compose.yaml file

Create the docker environment for the database and redis:
```bash
$ docker-compose up -d database redis
```

TODO: Place IAM Public Key


#### Install dependency

```bash
$ symfony composer install
```


#### Fresh Install
For new installation, do the following, existing installation can proceed to migration.

```bash
$  symfony console doctrine:database:drop --force
$  symfony console doctrine:database:create
$  symfony console make:migration
$  symfony console doctrine:migrations:migrate --no-interaction
```

Prepopulate the database with default content:
```bash
$  symfony console doctrine:fixtures:load --no-interaction
```

#### Migration
For existing database, do migration check and make migration if needed. if you have installed this before, you can make migration from previous release:
```bash
$  symfony console make:migration
```

Lastly, run the migration:
```bash
$  symfony console doctrine:migrations:migrate --no-interaction
```

Now your app are ready to use:

Landing page: [https://localhost:8080/](https://localhost:8080/)

API Endpoint and Docs: [https://localhost:8080/api](https://localhost:8080/api)

Admin page: [https://localhost:8080/admin](https://localhost:8080/admin)

default credentials:
```bash
root:toor
admin:admin
upk_pusat:upk_pusat
```

#### Test

Unit testing also available with the following command:

```bash
$ php bin/phpunit
```

## 4. Fully use native OS services

### Requirement
This method only require you to have the following tools:

1. [PHP Engine version 8.0.1 or newer](https://www.php.net)

2. [Symfony console](https://symfony.com/download)

3. [Postgre SQL version 13 or newer](https://www.postgresql.org/download/)

4. [Redis](https://redis.io/download)

### Installation
First, clone this repository:

```bash
$ git clone git@gitlab.com:dadangnh/hris-ref.git some_dir
$ cd some_dir
```

Then, create your environment by editing `.env` and save as `.env.local` or you can use OS's environment variable or use [Symfony Secrets](https://symfony.com/doc/current/configuration/secrets.html). Create your JWT passphrase on the JWT_PASSPHRASE key.

TODO: Place IAM Public Key

#### Install dependency

```bash
$ symfony composer install
```

#### Fresh Install
For new installation, do the following, existing installation can proceed to migration.

```bash
$  symfony console doctrine:database:drop --force
$  symfony console doctrine:database:create
$  symfony console make:migration
$  symfony console doctrine:migrations:migrate --no-interaction
```

Prepopulate the database with default content:
```bash
$  symfony console doctrine:fixtures:load --no-interaction
```

#### Migration
For existing database, do migration check and make migration if needed. if you have installed this before, you can make migration from previous release:
```bash
$  symfony console make:migration
```

Lastly, run the migration:
```bash
$  symfony console doctrine:migrations:migrate --no-interaction
```

Now your app are ready to use:

Landing page: [https://localhost:8080/](https://localhost:8080/)

API Endpoint and Docs: [https://localhost:8080/api](https://localhost:8080/api)

Admin page: [https://localhost:8080/admin](https://localhost:8080/admin)

default credentials:
```bash
root:toor
admin:admin
upk_pusat:upk_pusat
```

#### Test

Unit testing also available with the following command:

```bash
$ php bin/phpunit
```
